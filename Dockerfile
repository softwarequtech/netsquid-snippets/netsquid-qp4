FROM python:3.8

# Username and password obtained from https://forum.netsquid.org/ucp.php?mode=register
ARG USERNAME
ARG PASSWORD

RUN apt-get update && apt-get -y upgrade

RUN mkdir -p /opt/netsquid-qp4
WORKDIR /opt/netsquid-qp4

COPY Makefile requirements.txt test_requirements.txt ./

RUN NETSQUIDPYPI_USER=${USERNAME} NETSQUIDPYPI_PWD=${PASSWORD} make python-deps
RUN make test-deps
