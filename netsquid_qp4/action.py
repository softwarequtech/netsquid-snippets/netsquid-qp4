"""Behavioural model deparser."""

from netsquid_qp4 import expr


class Action:
    """A BM action."""

    def __init__(self, bm_action, extern):
        """
        Parameters
        ----------
        bm_action : dict
            Action definition from BMv2 JSON
        extern : P4 architecture extern
            Instance of architecture for extern calls - can be None if extern
            calls are not required
        """
        self._bm_action = bm_action
        self._extern = extern

    def send(self, bus, runtime_data):
        """
        Perform action.

        Parameters
        ----------
        bus : packet.Bus
            The metadata + headers bus
        """
        for prim in self._bm_action["primitives"]:
            if prim["op"] == "assign":
                assert len(prim["parameters"]) == 2
                left = expr.lval(bus, prim["parameters"][0], runtime_data)
                right = expr.rval(bus, prim["parameters"][1], runtime_data)
                left.val = right

            elif prim["op"] == "remove_header":
                assert len(prim["parameters"]) == 1
                param = prim["parameters"][0]
                assert param["type"] == "header"

                hdr_name = param["value"]
                if hdr_name in bus.packet:
                    hdr = bus.get_hdr(hdr_name)
                    hdr.set_invalid()

            elif prim["op"] == "add_header":
                assert len(prim["parameters"]) == 1
                param = prim["parameters"][0]
                assert param["type"] == "header"
                bus.packet.add_header(param["value"])

            else:
                assert self._extern is not None

                extern_func_name = prim["op"]
                extern_func = getattr(self._extern, extern_func_name)

                param_list = [
                    expr.param(bus, param, runtime_data)
                    for param in prim["parameters"]
                ]

                extern_func(*param_list)

    @property
    def name(self):
        return self._bm_action["name"]
