"""Abstract base classes for Device and Architecture."""

from abc import ABC


class Device(ABC):
    """Base class for P4 devices.

    A Device provides access to the runtime environment to the P4 architecture.
    An architecture will usually use the device to get metadata information
    that require access to the runtime (e.g. time). The device also defines the
    PortMeta object which is used to communicate port information to and from
    the architecture.

    """

    def __init__(self, arch):
        """
        Parameters
        ----------
        arch : Architecture
            The architecture this device is using
        """
        self._arch = arch

    @property
    def arch(self):
        return self._arch

    @property
    def model(self):
        return self._arch.model
