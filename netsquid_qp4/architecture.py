"""Abstract base class for P4 architectures."""

from abc import ABC, abstractmethod

from netsquid_qp4.bmv2 import BM


class Architecture(ABC):
    """Base class for P4 architectures.

    An architecture defines the packet processing pipeline, that is, how do the
    control blocks from the behavioural model connect to each other. It is also
    responsible for executing supporting tasks such as filling out metadata,
    dropping packets, replicating packets, etc.

    An architecture relies on a Runtime object to provide it with access to a
    runtime (e.g. a simulated NetSquid runtime).

    """

    def __init__(self, bm_json, runtime=None, extern_class=None):
        """
        Parameters
        ----------
        bm_json : str
            The BMv2 JSON file name
        runtime : Device
            The device runtime - can be `None` if not required
        extern_class : <architecture specific>
            The architecture extern class provided by the derived architecture
            class to be instantiated with the BM config by BM - can be `None`
            if externs are not required
        """
        self._runtime = runtime
        self._model = BM(bm_json, extern_class)

    @property
    def model(self):
        return self._model

    @abstractmethod
    def send(self, port_in_meta, packet_in):
        """
        Process incoming packet.

        The input packet is consumed and the output packet is a brand new
        object. The metadata headers are passed by reference and will reflect
        any changes made by BM.

        Parameters
        ----------
        port_in_meta : <architecture specific>
            Input port metadata
        packet_in : packet.RawPacket
            The raw input packet

        Returns
        -------
        (<architecture specific PortMeta>, packet.RawPacket)
            The output port metadata and the raw output packet
        """
        raise NotImplementedError
