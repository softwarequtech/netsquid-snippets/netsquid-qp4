"""Core module for the Behavioural model."""

import json

from netsquid_qp4.action import Action
from netsquid_qp4.deparser import Deparser
from netsquid_qp4.packet import BitVal, Header
from netsquid_qp4.parser import Parser
from netsquid_qp4.pipeline import Pipeline


class BM:
    "Representation of the Behavioural Model as read from its JSON."

    # TODO: @atomic annotation support
    #
    # P4 spec only guarantees that individual extern calls are executed
    # atomically. Multiple extern operations do not have such a guarantee.
    # Therefore, the P4 spec allows for an @atomic annotation to indicate
    # blocks that need to be executed atomically. However, it is not clear how
    # this is supported within BMv2 JSON (if at all).
    #
    # Note that this is not an issue for single threaded applications running
    # this code since all packets will be processed one by one anyway.

    def __init__(self, bm_json, extern_class=None):
        """
        Parameters
        ----------
        bm_json : str
            The BMv2 JSON file name
        extern_class : <architecture specific>
            The architecture extern class to be instantiated with the BM config
            - can be `None` if externs are not required
        """
        with open(bm_json) as bm_file:
            self._bm_config = json.load(bm_file)

        self._header_types = {
            hdr_t["name"]: hdr_t for hdr_t in self._bm_config["header_types"]
        }
        self._header_defs = {
            hdr["name"]: hdr for hdr in self._bm_config["headers"]
        }

        self._parsers = {
            pars["name"]: Parser(
                self._bm_config["parsers"][0],
                self._header_types,
                self._header_defs,
            ) for pars in self._bm_config["parsers"]
        }

        extern = (
            extern_class(self._bm_config) if extern_class is not None else None
        )
        self._actions = {
            act["id"]: Action(act, extern)
            for act in self._bm_config["actions"]
        }

        self._pipelines = {
            pl["name"]: Pipeline(pl, self._actions)
            for pl in self._bm_config["pipelines"]
        }

        self._deparsers = {
            depars["name"]: Deparser(self._bm_config["deparsers"][0])
            for depars in self._bm_config["deparsers"]
        }

    def standard_metadata(self):
        return self.header("standard_metadata")

    def scalars(self):
        return self.header("scalars")

    def header(self, header_name):
        assert header_name in self._header_defs
        header_type = self._header_defs[header_name]["header_type"]
        assert header_type in self._header_types

        header = Header()
        for field in self._header_types[header_type]["fields"]:
            field_name = field[0]
            field_bits = field[1]
            header[field_name] = BitVal(0, field_bits)

        return header

    @property
    def parsers(self):
        return self._parsers

    @property
    def pipelines(self):
        return self._pipelines

    @property
    def deparsers(self):
        return self._deparsers
