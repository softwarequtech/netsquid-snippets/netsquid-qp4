"""Behavioural model deparser."""

from netsquid_qp4.packet import RawPacket


class Deparser:
    """A BM deparser."""

    def __init__(self, bm_deparser):
        """
        Parameters
        ----------
        bm_deparser : dict
            Deparser definition from BMv2 JSON
        """
        self._bm_deparser = bm_deparser

    def send(self, packet):
        """
        Deparse a packet.

        Parameters
        ----------
        packet : packet.Packet
            The packet to deparse

        Returns
        -------
        packet.RawPacket
            The raw output packet.
        """
        packet_out = RawPacket()
        for hdr in reversed(self._bm_deparser["order"]):
            if packet.is_valid(hdr):
                packet_out.push(packet[hdr])

        return packet_out
