"""Parse expressions from BMv2 AST format."""

from collections import namedtuple
from enum import Enum, auto

ExprDispatch = namedtuple("ExprDispatch", "function can_be_lval")

OperDispatch = namedtuple("OperDispatch", "function nr_args")


class ExprContext(Enum):
    LVAL = auto()
    RVAL = auto()
    PARAM = auto()


def evaluate(bus, expr, runtime_data, expr_context=ExprContext.RVAL):
    if runtime_data is None:
        runtime_data = []
    if "op" in expr:
        dispatch = OPER_DISPATCHES.get(expr["op"])
        check_nr_args(expr, dispatch.nr_args)
        return dispatch.function(bus, expr, runtime_data)

    assert "type" in expr
    dispatch = EXPR_DISPATCHES.get(
        expr["type"],
        ExprDispatch(_expr_value, False),
    )
    is_lval = (
        True if expr_context == ExprContext.LVAL else
        False if expr_context == ExprContext.RVAL else
        dispatch.can_be_lval
    )
    if not dispatch.can_be_lval:
        assert not is_lval
    return dispatch.function(bus, expr, runtime_data, is_lval)


def check_nr_args(expr, nr_args):
    assert nr_args in [1, 2, 3]
    assert expr["right"] is not None
    if nr_args >= 2:
        assert expr["left"] is not None
    else:
        assert expr["left"] is None
    if nr_args >= 3:
        assert expr["cond"] is not None
    else:
        assert ("cond" not in expr) or (expr["cond"] is None)


def lval(bus, expr, runtime_data):
    return evaluate(bus, expr, runtime_data, ExprContext.LVAL)


def rval(bus, expr, runtime_data):
    return evaluate(bus, expr, runtime_data, ExprContext.RVAL)


def param(bus, expr, runtime_data):
    return evaluate(bus, expr, runtime_data, ExprContext.PARAM)


def _expr_expression(bus, expr, runtime_data, is_lval):
    return evaluate(bus, expr["value"], runtime_data, is_lval)


def _expr_field(bus, expr, _runtime_data, is_lval):
    (header_instance_name, field_member_name) = expr["value"]
    if is_lval:
        header = bus.get_hdr(header_instance_name)
        return header[field_member_name]
    else:
        if field_member_name == "$valid$":
            return bus.packet.is_valid(header_instance_name)
        else:
            header = bus.get_hdr(header_instance_name)
            return header[field_member_name].val


def _expr_hexstr(_bus, expr, _runtime_data, _is_lval):
    return int(expr["value"], 0)


def _expr_header(bus, expr, _runtime_data, _is_lval):
    value = expr["value"]
    return bus.get_hdr(value)


def _expr_bool(_bus, expr, _runtime_data, _is_lval):
    value = expr["value"]
    assert isinstance(value, bool)
    return value


def _expr_register(_bus, _expr, _runtime_data, _is_lval):
    # TODO
    raise NotImplementedError


def _expr_header_stack(_bus, _expr, _runtime_data, _is_lval):
    # TODO
    raise NotImplementedError


def _expr_stack_field(_bus, _expr, _runtime_data, _is_lval):
    # TODO
    raise NotImplementedError


def _expr_runtime_data(_bus, expr, runtime_data, _is_lval):
    index = expr["value"]
    value_str = runtime_data[index]
    value = int(value_str, 16)
    return value


def _expr_value(_bus, expr, _runtime_data, _is_lval):
    assert expr["type"] in [
        "meter_array",
        "counter_array",
        "register_array",
    ]
    return expr["value"]


EXPR_DISPATCHES = {
    "expression": ExprDispatch(_expr_expression, True),
    "field": ExprDispatch(_expr_field, True),
    "hexstr": ExprDispatch(_expr_hexstr, False),
    "header": ExprDispatch(_expr_header, False),
    "bool": ExprDispatch(_expr_bool, False),
    # TODO: check can_be_is_lval for "register"
    "register": ExprDispatch(_expr_register, True),
    "header_stack": ExprDispatch(_expr_header_stack, False),
    "stack_field": ExprDispatch(_expr_stack_field, False),
    "runtime_data": ExprDispatch(_expr_runtime_data, False),
    "local": ExprDispatch(_expr_runtime_data, False),
}


def _oper_add(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value + right_value


def _oper_subtract(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value - right_value


def _oper_multiply(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value * right_value


def _oper_left_shift(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value << right_value


def _oper_right_shift(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value >> right_value


def _oper_is_equal(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value == right_value


def _oper_is_not_equal(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value != right_value


def _oper_is_greater_than(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value > right_value


def _oper_is_greater_than_or_equal(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value >= right_value


def _oper_is_less_than(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value < right_value


def _oper_is_less_than_or_equal(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value <= right_value


def _oper_logical_and(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, bool)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, bool)
    return left_value and right_value


def _oper_logical_or(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, bool)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, bool)
    return left_value or right_value


def _oper_logical_not(bus, expr, runtime_data):
    value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(value, bool)
    return not value


def _oper_bitwise_and(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value & right_value


def _oper_bitwise_or(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value | right_value


def _oper_bitwise_xor(bus, expr, runtime_data):
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value ^ right_value


def _oper_bitwise_not(bus, expr, runtime_data):
    value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(value, int)
    return ~value


def _oper_is_valid(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_is_valid_union(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_data_to_bool(bus, expr, runtime_data):
    value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(value, int)
    return bool(value)


def _oper_bool_to_data(bus, expr, runtime_data):
    value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(value, bool)
    return int(value)


def _oper_two_comp_mod(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_sat_cast(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_usat_cast(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_ternary(bus, expr, runtime_data):
    cond_value = evaluate(bus, expr["cond"], runtime_data)
    assert isinstance(cond_value, bool)
    left_value = evaluate(bus, expr["left"], runtime_data)
    assert isinstance(left_value, int)
    right_value = evaluate(bus, expr["right"], runtime_data)
    assert isinstance(right_value, int)
    return left_value if cond_value else right_value


def _oper_deref_header_stack(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_last_stack_index(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_size_stack(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_access_field(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_dereference_union_stack(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


def _oper_access_union_header(_bus, _expr, _runtime_data):
    # TODO
    raise NotImplementedError


OPER_DISPATCHES = {
    "+": OperDispatch(_oper_add, 2),
    "-": OperDispatch(_oper_subtract, 2),
    "*": OperDispatch(_oper_multiply, 2),
    "<<": OperDispatch(_oper_left_shift, 2),
    ">>": OperDispatch(_oper_right_shift, 2),
    "==": OperDispatch(_oper_is_equal, 2),
    "!=": OperDispatch(_oper_is_not_equal, 2),
    ">": OperDispatch(_oper_is_greater_than, 2),
    ">=": OperDispatch(_oper_is_greater_than_or_equal, 2),
    "<": OperDispatch(_oper_is_less_than, 2),
    "<=": OperDispatch(_oper_is_less_than_or_equal, 2),
    "and": OperDispatch(_oper_logical_and, 2),
    "or": OperDispatch(_oper_logical_or, 2),
    "not": OperDispatch(_oper_logical_not, 1),
    "&": OperDispatch(_oper_bitwise_and, 2),
    "|": OperDispatch(_oper_bitwise_or, 2),
    "^": OperDispatch(_oper_bitwise_xor, 2),
    "~": OperDispatch(_oper_bitwise_not, 1),
    "valid": OperDispatch(_oper_is_valid, 1),
    "valid_union": OperDispatch(_oper_is_valid_union, 1),
    "d2b": OperDispatch(_oper_data_to_bool, 1),
    "b2d": OperDispatch(_oper_bool_to_data, 1),
    "two_comp_mod": OperDispatch(_oper_two_comp_mod, 2),
    "sat_cast": OperDispatch(_oper_sat_cast, 1),
    "usat_cast": OperDispatch(_oper_usat_cast, 1),
    "?": OperDispatch(_oper_ternary, 3),
    # TODO: check nr args in all of the following
    "dereference_header_stack": OperDispatch(_oper_deref_header_stack, 1),
    "last_stack_index": OperDispatch(_oper_last_stack_index, 1),
    "size_stack": OperDispatch(_oper_size_stack, 1),
    "access_field": OperDispatch(_oper_access_field, 1),
    "dereference_union_stack": OperDispatch(_oper_dereference_union_stack, 1),
    "access_union_header": OperDispatch(_oper_access_union_header, 1),
}
