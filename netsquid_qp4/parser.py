"""Behavioural model parser."""

from netsquid_qp4 import expr
from netsquid_qp4.packet import Bus, Packet


class Parser:
    """A BM parser."""

    def __init__(self, bm_parser, header_types, header_defs):
        """
        Parameters
        ----------
        bm_parser : dict
            Parser definition from BMv2 JSON
        header_types : dict of {header_type["name"]: header_type}
            Dictionary of header types
        header_defs : dict of {header_def["name"]: header_def}
            Dictionary of header definitions ("headers" in BMv2 JSON)
        """
        self._bm_parser = bm_parser
        self._header_types = header_types
        self._header_defs = header_defs

        # The ParseState class is the actual work horse of the parser.
        self._states = {
            parse_state["name"]: ParseState(parse_state)
            for parse_state in self._bm_parser["parse_states"]
        }

    def send(self, standard_metadata, scalars, packet_in):
        """
        Parse incoming packet.

        Parameters
        ----------
        standard_metadata : packet.Header
            The architecture defined metadata
        scalars : packet.Header
            Scalar variables
        packet_in : packet.RawHeader
            The raw input header

        Returns
        -------
        Bus
            The metadata + headers bus
        """

        packet = Packet(self._header_types, self._header_defs)
        bus = Bus(standard_metadata, scalars, packet)

        state = self._bm_parser["init_state"]
        while state is not None:
            state = self._states[state].send(packet_in, bus)

        return bus


class ParseState:
    """A parser state."""

    def __init__(self, bm_parse_state):
        """
        Parameters
        ----------
        bm_parse_state : dict
            Parse state definition from BMv2 JSON
        """
        self._bm_parse_state = bm_parse_state

    def send(self, packet_in, bus):
        """
        Process parser state for incoming packet.

        Parameters
        ----------
        packet_in : packet.RawPacket
            The raw input packet
        bus : packet.Bus
            The metadata+headers bus

        Returns
        -------
        str
            The name of the next state
        """
        for op in self._bm_parse_state["parser_ops"]:
            if op["op"] == "extract":
                assert packet_in
                assert len(op["parameters"]) == 1
                header_name = op["parameters"][0]["value"]
                bus.packet[header_name] = packet_in.pop()
            else:
                # Eventually all operations should be supported
                raise NotImplementedError

        transition_key = None
        if self._bm_parse_state["transition_key"]:
            # More than one key is legal, but not sure what that means
            assert len(self._bm_parse_state["transition_key"]) == 1
            transition_key = self._bm_parse_state["transition_key"][0]
            transition_key_val = expr.rval(bus, transition_key, None)

        for transition in self._bm_parse_state["transitions"]:
            if transition["type"] == "default":
                return transition["next_state"]

            rval = expr.rval(bus, transition, None)
            if transition_key_val == rval:
                return transition["next_state"]

        # Should not get here
        raise RuntimeError
