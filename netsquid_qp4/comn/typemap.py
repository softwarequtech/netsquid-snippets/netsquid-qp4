"""Type mappings for NetConf's builder."""

from netsquid_qp4.devices.connections import (
    ClassicalConnection,
    QuantumConnection,
    CombinedConnection,
)
from netsquid_qp4.devices.midpoint import MidPoint
from netsquid_qp4.devices.switch import Switch
from netsquid_qp4.devices.qnode import QNode

from .netconf import ComponentBuilder


def setup_builder_type_map():
    ComponentBuilder.add_type("classical_connection", ClassicalConnection)
    ComponentBuilder.add_type("quantum_connection", QuantumConnection)
    ComponentBuilder.add_type("combined_connection", CombinedConnection)
    ComponentBuilder.add_type("midpoint", MidPoint)
    ComponentBuilder.add_type("switch", Switch)
    ComponentBuilder.add_type("qnode", QNode)
