"""Logging for QP4."""

import contextlib
import logging
import os
import sys

import netsquid as ns

LEVEL = logging.DEBUG
LOG_CONSOLE = False
LOG_FILE = True


ROOT_LOGGER = logging.getLogger('root')


class Formatter(logging.Formatter):
    # pylint:disable=missing-class-docstring
    def formatTime(self, record, datefmt=None):
        super_self = super().formatTime(record, datefmt)
        return super_self + " sim: {:>12} ns".format(int(ns.sim_time()))


def setup_logging(
        logger=ROOT_LOGGER,
        log_console=LOG_CONSOLE,
        log_file=LOG_FILE,
):
    logger.setLevel(LEVEL)

    if log_console:
        fmt = ("%(asctime)s  ::  "
               "%(name)-6s  ::  "
               "%(module)-10s  ::  "
               "%(levelname)-7s  ::  "
               "%(message)s")
        formatter = Formatter(fmt)

        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    if log_file:
        log_dir_name = "logs"
        log_file_name = os.path.join(
            log_dir_name,
            "{}.log".format(logger.name),
        )

        if not os.path.exists(log_dir_name):
            os.makedirs(log_dir_name)

        with contextlib.suppress(FileNotFoundError):
            os.remove(log_file_name)

        fmt = ("%(asctime)s  ::  "
               "%(module)-10s  ::  "
               "%(levelname)-7s  ::  "
               "%(message)s")
        formatter = Formatter(fmt)

        handler = logging.FileHandler(log_file_name)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
