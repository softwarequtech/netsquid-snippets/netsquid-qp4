"""Network configuration module."""

from abc import ABC, abstractclassmethod
import os
import yaml

from netsquid.components.component import Component


# pylint: disable=R0901
# Reason: ancestors defined in library.
class Loader(yaml.SafeLoader):
    """Loader class to allow for !include statement in config."""

    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super().__init__(stream)

    def include(self, node):
        """Process and include statement."""
        filename = os.path.join(self._root, self.construct_scalar(node))

        with open(filename, 'r') as file_handle:
            return yaml.load(file_handle, Loader)


class NetConf:
    """Python instantiation of a configuration read from a file.

    Parameters
    ----------
    config_file : string
        File name of the YAML configuration file.
    extra_builders : list of Builder
        (Optional) Additional builders for the provided configuration.
    extra_linkers : list of Linker
        (Optional) Additional linkers for the provided configuration.
    """

    def __init__(self, config_file, extra_builders=None, extra_linkers=None):
        builders = [ComponentBuilder] + \
            ([] if extra_builders is None else extra_builders)
        linkers = [ComponentLinker] + \
            ([] if extra_linkers is None else extra_linkers)

        # So that we can use !includes in the files
        Loader.add_constructor('!include', Loader.include)

        config = yaml.load(open(config_file, 'r'), Loader)

        self._objects = {}

        self._objects = {
            builder.key(): builder.build(config[builder.key()])
            for builder in filter(lambda b: b.key() in config, builders)
        }

        for linker in linkers:
            linker.link(self._objects, config)

    @property
    def objects(self):
        """
        Returns
        -------
        dict
            Dictionary of configured objects.
        """
        return self._objects

    @property
    def components(self):
        """
        Returns
        -------
        dict
            Dictionary of configured NetSquid Components.
        """
        return self._objects["components"]


class Builder(ABC):
    """Abstract base class for NetConf builders."""

    @abstractclassmethod
    def key(cls):
        """Keyword for the configuration subset handled by this builder."""
        raise NotImplementedError()

    @abstractclassmethod
    def types(cls):
        """The name to class mapping to be used by the builder."""
        raise NotImplementedError()

    @abstractclassmethod
    def add_type(cls, name, new_type):
        """Add a new name to class mapping to this builder class.

        Parameters
        ----------
        name : str
            The name of the new type.
        new_type : Class
            The class to use for instantiating the new type.
        """
        raise NotImplementedError()

    @abstractclassmethod
    def build(cls, config_dict):
        """Build the objects identified in the configuration.

        Parameters
        ----------
        config_dict : dict
            The configuration for this builder to process.

        Returns
        -------
        collection
            A collection of objects. Usually a list or dict.
        """
        raise NotImplementedError()


class ComponentBuilder(Builder):
    """Builder for NetSquid components."""

    TYPES = {}

    @classmethod
    def key(cls):
        return "components"

    @classmethod
    def types(cls):
        return cls.TYPES

    @classmethod
    def add_type(cls, name, new_type):
        assert issubclass(new_type, Component)
        cls.TYPES[name] = new_type

    @classmethod
    def build(cls, config_dict):
        components = {}

        for name, comp in config_dict.items():
            # Extract properties if any
            properties = comp["properties"] if "properties" in comp else {}

            # Create the component object
            component = cls.TYPES[comp["type"]](name, **properties)

            # Create and add its sub-components recursively
            if "components" in comp:
                for sub_comp in cls.build(comp["components"]).values():
                    component.add_component(sub_comp)

            components[name] = component

        return components


# pylint: disable=R0903
# Reason: One public method is fine - need an ABC
class Linker(ABC):
    """Abstract base class for NetConf linkers."""

    @abstractclassmethod
    def link(cls, object_dict, conf_dict):
        """Link the objects created by the builders.

        Parameters
        ----------
        object_dict : dict
            Full dictionary of objects created by the builder.
        conf_dict : dict
            The configuration for this linker to process.
        """
        raise NotImplementedError()


class ComponentLinker(Linker):
    """Linker for NetSquid components. Relies on the Port abstraction."""

    @classmethod
    def _get_port(cls, port_name, comp):
        if port_name not in comp.ports:
            raise ValueError("Component {} does not have port {}"
                             .format(comp.name, port_name))
        return comp.ports[port_name]

    @classmethod
    def _get_remote_comp(cls, remote_name, comp, peer_comps):
        # Is it a peer component?
        if remote_name in peer_comps:
            return peer_comps[remote_name]

        # If not, is it a sub-component?
        if remote_name in comp.subcomponents:
            return comp.subcomponents[remote_name]

        raise ValueError(
            "Component {}: remote {} is not a peer or sub-component"
            .format(comp.name, remote_name)
        )

    @classmethod
    def _get_remote_comp_conf(
            cls,
            remote_name,
            comp_name,
            comp_conf,
            peer_comps_conf
    ):
        if remote_name in peer_comps_conf:
            return peer_comps_conf[remote_name]

        if remote_name in comp_conf["components"]:
            return comp_conf["components"][remote_name]

        raise ValueError(
            "Component {}: "
            "remote {} is not configured as a peer or sub-component"
            .format(comp_name, remote_name)
        )

    @classmethod
    def _verify_remote_port_conf(
            cls,
            comp_name,
            port_name,
            port_conf,
            remote_port_conf
    ):
        if remote_port_conf["remote"] != comp_name or \
           remote_port_conf["port"] != port_name:
            raise ValueError(
                "Component {}, port {}: "
                "remote port {} on component {} does not agree "
                "that it is the remote"
                .format(
                    comp_name,
                    port_name,
                    port_conf["port"],
                    port_conf["remote"],
                )
            )

    @classmethod
    def _recursive_link(cls, object_dict, conf_dict):
        for comp_name, comp_conf in conf_dict.items():
            if "ports" not in comp_conf:
                continue

            for port_name, port_conf in comp_conf["ports"].items():

                # Get the component and its port
                comp = object_dict[comp_name]
                port = cls._get_port(port_name, comp)

                # If port already connected, skip
                if port.is_connected:
                    continue

                # Find the remote component and its port
                remote_comp = cls._get_remote_comp(
                    port_conf["remote"], comp, object_dict,
                )
                remote_port = cls._get_port(port_conf["port"], remote_comp)

                # Find the configuration for the remote port (if it exists). If
                # it exists, it must agree that this is the port it wants to
                # connect to.
                remote_comp_conf = cls._get_remote_comp_conf(
                    remote_comp.name,
                    comp_name,
                    comp_conf,
                    conf_dict,
                )
                remote_port_conf = remote_comp_conf.get(remote_port.name)

                if remote_port_conf is not None:
                    cls._verify_remote_port_conf(
                        comp_name,
                        port_name,
                        port_conf,
                        remote_port_conf
                    )

                # Finally, connect
                port.connect(remote_port)

            # Recursively descend into sub-components
            if "components" in comp_conf:
                assert comp.subcomponents
                cls._recursive_link(
                    comp.subcomponents,
                    comp_conf["components"],
                )

    @classmethod
    def link(cls, object_dict, conf_dict):
        if "components" not in object_dict:
            return

        cls._recursive_link(
            object_dict["components"],
            conf_dict["components"]
        )
