"""A behavioural model pipeline."""

from netsquid_qp4 import expr
from netsquid_qp4.table import Table


class Pipeline:
    """A BM pipeline."""

    def __init__(self, bm_pipeline, actions):
        """
        Parameters
        ----------
        bm_pipeline : dict
            Pipeline definition from BMv2 JSON
        actions : dict of {action_id: Action}
            The actions from the BMv2 JSON
        """
        self._bm_pipeline = bm_pipeline
        self._actions = actions

        self._action_name_to_id = {}
        for act_id, act in self._actions.items():
            self._action_name_to_id[act.name] = act_id

        # Separate tables and conditionals as that's what BMv2 does
        self._tables = {
            tab["name"]: Table(tab, self._action_name_to_id)
            for tab in self._bm_pipeline["tables"]
        }
        self._conditionals = {
            cond["name"]: cond for cond in self._bm_pipeline["conditionals"]
        }

    @property
    def tables(self):
        return self._tables

    def _conditional(self, bm_conditional, bus):
        # pylint:disable=no-self-use
        """
        Apply a conditional.

        Parameters
        ----------
        bm_conditional : dict
            Conditional definition from BMv2 JSON
        bus : packet.Bus
            The metadata + headers bus

        Returns
        -------
        str
            The next table
        """
        cond_expression = bm_conditional["expression"]
        result = expr.rval(bus, cond_expression, None)
        assert result is not None
        if result:
            return bm_conditional["true_next"]
        else:
            return bm_conditional["false_next"]

    def _table(self, table, bus):
        """
        Apply a table.

        Parameters
        ----------
        table : table.Table
            The table to apply
        bus : packet.Bus
            The metadata + headers bus

        Returns
        -------
        str
            The next table
        """
        action_entry = table.match_packet(bus)
        action_id = action_entry["action_id"]
        action_data = action_entry["action_data"]
        action = self._actions[action_id]
        action.send(bus, action_data)
        return table.next_table(action)

    def send(self, bus):
        """
        Process the pipeline.

        Parameters
        ----------
        bus : packet.Bus
            The metadata + headers bus
        """
        next_table = self._bm_pipeline["init_table"]

        while next_table is not None:
            if next_table in self._tables:
                next_table = self._table(self._tables[next_table], bus)
            else:
                assert next_table in self._conditionals
                next_table = self._conditional(
                    self._conditionals[next_table],
                    bus,
                )
