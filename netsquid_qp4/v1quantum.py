"""V1Quantum architecture and device definition."""

from abc import ABC, abstractmethod
from collections import namedtuple
from functools import partial

from netsquid_qp4.architecture import Architecture
from netsquid_qp4.packet import BitVal


class V1QuantumArchitecture(Architecture):

    def __init__(self, bm_json, runtime):
        # This architecture requires a runtime
        assert runtime is not None

        super().__init__(bm_json, runtime, partial(V1QuantumExtern, runtime=runtime))

        # Verify architecture specific properties
        assert len(self.model.parsers) == 1
        assert "parser" in self.model.parsers

        assert len(self.model.pipelines) == 2
        assert "ingress" in self.model.pipelines
        assert "egress" in self.model.pipelines

        assert len(self.model.deparsers) == 1
        assert "deparser" in self.model.deparsers

    @property
    def _parser(self):
        return self.model.parsers["parser"]

    @property
    def _ingress(self):
        return self.model.pipelines["ingress"]

    @property
    def ingress_tables(self):
        return self.model.pipelines["ingress"].tables

    @property
    def _egress(self):
        return self.model.pipelines["egress"]

    @property
    def egress_tables(self):
        return self.model.pipelines["egress"].tables

    @property
    def _deparser(self):
        return self.model.deparsers["deparser"]

    def send(self, port_in_meta, packet_in):
        standard_metadata = self.model.standard_metadata()
        scalars = self.model.scalars()

        # ---------------------------------------------------------------------
        # Parser
        # ---------------------------------------------------------------------

        standard_metadata["ingress_port"].val = port_in_meta.port_num
        bus = self._parser.send(standard_metadata, scalars, packet_in)

        # ---------------------------------------------------------------------
        # Ingress
        # ---------------------------------------------------------------------

        bus.standard_metadata["ingress_global_timestamp"].val = int(self._runtime.time())

        self._ingress.send(bus)
        mp_mcast = (
            bus.standard_metadata["mp_egress_spec_1"].val != 0 and
            bus.standard_metadata["mp_egress_spec_2"].val != 0
        )
        if (bus.standard_metadata["egress_spec"] is None) and (not mp_mcast):
            bus.packet.clear()

        # ---------------------------------------------------------------------
        # Replication
        # ---------------------------------------------------------------------

        bus_list = []
        if mp_mcast:
            bus_1 = bus
            bus_2 = bus.clone()

            bus_1.standard_metadata["egress_spec"] = (bus_1.standard_metadata["mp_egress_spec_1"])
            bus_1.standard_metadata["other_port"].val = (
                bus_1.standard_metadata["mp_egress_spec_2"].val
            )

            bus_2.standard_metadata["egress_spec"] = (bus_2.standard_metadata["mp_egress_spec_2"])
            bus_2.standard_metadata["other_port"].val = (
                bus_2.standard_metadata["mp_egress_spec_1"].val
            )

            bus_list.append(bus_1)
            bus_list.append(bus_2)

        elif bus.standard_metadata["egress_spec"] is not None:
            bus_list.append(bus)

        # ---------------------------------------------------------------------
        # Egress
        # ---------------------------------------------------------------------

        for bus in bus_list:
            bus.standard_metadata["egress_port"].val = (bus.standard_metadata["egress_spec"].val)
            bus.standard_metadata["egress_global_timestamp"].val = int(self._runtime.time())

            self._egress.send(bus)
            if bus.standard_metadata["egress_spec"] is None:
                bus.packet.clear()

        # ---------------------------------------------------------------------
        # Deparser
        # ---------------------------------------------------------------------

        bus_packet_out_list = [(bus, self._deparser.send(bus.packet)) for bus in bus_list]

        # ---------------------------------------------------------------------
        # Emit
        # ---------------------------------------------------------------------

        port_packet_out = []
        for bus, packet_out in bus_packet_out_list:
            if packet_out:
                port_out_meta = V1QuantumPortMeta(port_num=bus.standard_metadata["egress_spec"].val)
                port_packet_out.append((port_out_meta, packet_out))

        return port_packet_out


V1QuantumPortMeta = namedtuple(
    "PortMeta",
    [
        "port_num",
    ]
)


class V1QuantumRuntime(ABC):

    @abstractmethod
    def time(self):
        """
        Returns
        -------
        int
            The current time on the device in microseconds. The clock must be
            set to 0 every time the switch starts.
        """
        raise NotImplementedError

    def generate(self, port):
        """
        Generate spin-photon state at given port.

        Parameters
        ----------
        port : int
            Port through which the photon is to be emitted.
        """
        raise NotImplementedError


class V1QuantumExtern:

    def __init__(self, bm_config, runtime=None):
        self._registers = {reg["name"]: Register(reg) for reg in bm_config["register_arrays"]}
        self._runtime = runtime

    def mark_to_drop(self, standard_metadata):
        # pylint:disable=no-self-use
        standard_metadata["egress_spec"] = None

    def register_read(self, lval, register_name, index):
        lval.val = self._registers[register_name][int(index)].val

    def register_write(self, register_name, index, rval):
        self._registers[register_name][int(index)].val = int(rval)

    def generate_spin_photon(self, port):
        self._runtime.generate(port)

    def mp_gen_response(self, standard_metadata, port_1, port_2):
        # pylint:disable=no-self-use
        standard_metadata["egress_spec"] = None
        standard_metadata["mp_egress_spec_1"].val = port_1.val if hasattr(port_1, "val") else port_1
        standard_metadata["mp_egress_spec_2"].val = port_2.val if hasattr(port_2, "val") else port_2


class Register:

    def __init__(self, bm_register):
        self._bm_register = bm_register

        self._entries = []
        for _ in range(self._bm_register["size"]):
            self._entries.append(BitVal(0, self._bm_register["bitwidth"]))

    def __getitem__(self, index):
        return self._entries[index]
