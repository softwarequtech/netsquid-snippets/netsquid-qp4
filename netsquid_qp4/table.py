"""A match+action table for the Behavioural Model."""


class Table:
    """A table for match+action rules."""

    def __init__(self, bm_table, action_name_to_id):
        """
        Parameters
        ----------
        bm_table : dict
            Table definition from BMv2 JSON
        action_name_to_id : dict
            Map of action names to their IDs
        """
        self._bm_table = bm_table
        self._action_name_to_id = action_name_to_id

    def match_packet(self, bus):
        """
        Get the action for the provided packet.

        Parameters
        ----------
        bus : packet.Bus
            The metadata + headers bus

        Returns
        -------
        action_entry = {'action_id': int, 'action_data': [str]}

        TODO: Add support for with_counters
        TODO: Add support for support_timeout
        TODO: Add support for direct_meters
        """
        key_value = self._extract_key_value_from_bus(bus)
        action_entry = self._lookup_key_value(key_value)
        # TODO: Also return the action data
        return action_entry

    def insert_entry(self, key, action_name, action_data):
        new_entry = {
            "match_key": [
                {
                    "match_type": self._bm_table["match_type"],
                    "key": hex(key),
                },
            ],
            "action_entry": {
                "action_id": self._action_name_to_id[action_name],
                "action_data": [hex(item) for item in action_data],
            },
            "priority": 1,
        }

        if "entries" not in self._bm_table:
            self._bm_table["entries"] = []

        for tab_entry in self._bm_table["entries"]:
            assert new_entry["match_key"] != tab_entry["match_key"]

        self._bm_table["entries"].append(new_entry)

    def _extract_key_value_from_bus(self, bus):
        key_value = []
        for key_elem in self._bm_table["key"]:
            [header_name, field_name] = key_elem["target"]
            # TODO: Add support for key elem masks
            field = bus.get_hdr(header_name)[field_name]
            key_elem_value = {}
            key_elem_value["match_type"] = key_elem["match_type"]
            key_elem_value["value"] = field
            key_value.append(key_elem_value)
        return key_value

    def _lookup_key_value(self, key_value):
        # Do a brute-force lookup, checking each entry. For the simulated
        # quantum soft-switch tables are small and performance is not
        # critical (NetSquid is not fast anyway). If performance was more
        # important, we could have represented the table in a more optimized
        # way, e.g. a Patricia tree for LPM tables.
        # pylint:disable=unsubscriptable-object
        best_entry = None
        for entry in self._bm_table.get("entries", []):
            if Table._key_value_matches_entry(key_value, entry):
                if (best_entry is None or
                        entry["priority"] < best_entry["priority"]):
                    best_entry = entry
        if best_entry:
            return best_entry["action_entry"]
        # We could not find any matching entry; return the default action.
        action_id = self._bm_table["default_entry"]["action_id"]
        action_data = self._bm_table["default_entry"]["action_data"]
        return {"action_id": action_id, "action_data": action_data}

    @staticmethod
    def _key_value_matches_entry(key_value, entry):
        match_key = entry["match_key"]
        assert len(key_value) == len(match_key)
        for key_value_elem, match_elem in zip(key_value, match_key):
            if not Table._key_elem_matches_entry(key_value_elem, match_elem):
                return False
        return True

    @staticmethod
    def _key_elem_matches_entry(key_value_elem, match_elem):
        assert key_value_elem["match_type"] == match_elem["match_type"]
        match_type = key_value_elem["match_type"]
        if match_type == "exact":
            return Table._exact_key_elem_matches_entry(key_value_elem, match_elem)
        if match_type == "ternary":
            raise NotImplementedError
            # return Table._ternary_key_elem_matches_entry(key_value_elem, match_elem)
        if match_type == "lpm":
            return Table._lpm_key_elem_matches_entry(key_value_elem, match_elem)
        # There is no other match_type
        raise RuntimeError

    @staticmethod
    def _exact_key_elem_matches_entry(key_elem, match_elem):
        field_value = key_elem["value"].val
        match_value = int(match_elem["key"], 16)
        return field_value == match_value

    @staticmethod
    def _ternary_key_elem_matches_entry(_key_elem, _match_elem):
        # TODO implement this
        raise NotImplementedError

    @staticmethod
    def _lpm_key_elem_matches_entry(key_elem, match_elem):
        field = key_elem["value"]
        field_len = field.bits
        prefix_len = match_elem['prefix_length']
        mask = Table._prefix_to_mask(field_len, prefix_len)
        field_value = field.val & mask
        match_value = int(match_elem["key"], 16)
        return field_value == match_value

    @staticmethod
    def _prefix_to_mask(field_len, prefix_len):
        """
        Convert a prefix length into a mask (which requires knowing the field
        length).
        """
        assert prefix_len <= field_len
        mask = 0
        leading_ones = prefix_len
        for _ in range(leading_ones):
            mask = (mask << 1) | 1
        trailing_zeros = field_len - prefix_len
        for _ in range(trailing_zeros):
            mask <<= 1
        return mask

    def next_table(self, action):
        """
        Determine the next table, given that we executed action for this table.

        Parameters
        ----------
        action : Action
            The action that was performed for this table

        Returns
        -------
        Table
            The next table
        """
        action_name = action.name
        next_tables = self._bm_table["next_tables"]
        return next_tables.get(
            action_name,
            self._bm_table["base_default_next"],
        )
