"""Structures representing packets."""

from copy import deepcopy


class BitVal:
    """A finite-bit unsigned integer."""

    def __init__(self, value, bits):
        """
        Parameters
        ----------
        value : int
            Initial value
        bits : int
            Number of bits
        """
        self._bits = bits
        self._mask = (1 << self._bits) - 1
        self._value = None

        self.val = value

    def __repr__(self):
        return str(self.val)

    def __eq__(self, other):
        return ((self._bits == other._bits) and
                (self._mask == other._mask) and
                (self._value == other._value))

    def __int__(self):
        return self._value

    @property
    def bits(self):
        return self._bits

    @property
    def val(self):
        return self._value

    @val.setter
    def val(self, value):
        assert isinstance(value, int)
        assert value == (value & self._mask)
        self._value = value


class Header:
    """
    A single packet header.

    Unlike proper packets on a wire, a header is a dictionary of {name: value}
    """

    def __init__(self):
        self._fields = {}
        self._valid = True

    def __repr__(self):
        return str(self._fields)

    def __len__(self):
        return len(self._fields)

    def __contains__(self, name):
        return name in self._fields

    def __getitem__(self, name: str) -> BitVal:
        return self._fields[name]

    def __setitem__(self, name: str, value: BitVal):
        self._fields[name] = value

    @property
    def valid(self):
        return self._valid

    def set_valid(self):
        self._valid = True

    def set_invalid(self):
        self._valid = False


class RawPacket:
    """
    A raw packet.

    A raw packet is an ordered stack of headers (of type Header).
    """

    def __init__(self):
        self._stack = []

    def __repr__(self):
        return str(self._stack)

    def __len__(self):
        return len(self._stack)

    def push(self, header):
        self._stack.append(header)

    def pop(self):
        return self._stack.pop()


class Packet:
    """
    A BM internal representation of a packet.

    This packet representation is internal only as it checks for correct packet
    structure based on the installed P4 program. It is also unordered and
    stores headers by name (as defined in P4 program).
    """

    def __init__(self, header_types, header_defs):
        """
        Parameters
        ----------
        header_types : dict of {header_type["name"]: header_type}
            Dictionary of header types
        header_defs : dict of {header_def["name"]: header_def}
            Dictionary of header definitions ("headers" in BMv2 JSON)
        """
        self._header_types = header_types
        self._header_defs = header_defs

        self._headers = {}

    def __contains__(self, name):
        return name in self._headers

    def __getitem__(self, name):
        return self._headers[name]

    def __setitem__(self, name, header):
        # Header must be one defined in BMv2 JSON.
        assert name in self._header_defs

        # Find header type and field list to verify provided header.
        header_type = self._header_defs[name]["header_type"]
        header_fields = self._header_types[header_type]["fields"]

        # We will copy the entire header to ensure the BM internals do not have
        # to worry about whether they're dealing with a copy or reference.
        header_copy = Header()

        # Check that the provided header matches the definition from BMv2.
        assert len(header) == len(header_fields)
        for field in header_fields:
            field_name = field[0]
            field_bits = field[1]

            field_value = header[field_name]
            mask = (1 << field_bits) - 1

            # We verify both, that the value is indeed legal and that the
            # BitVal struct agrees as to the number of bits.
            assert (field_value.val & mask) == field_value.val
            assert field_value.bits == field_bits

            # And create a copy
            header_copy[field_name] = BitVal(field_value.val, field_value.bits)

        self._headers[name] = header_copy

    def add_header(self, name):
        # Header must be one defined in BMv2 JSON.
        assert name in self._header_defs

        # If the header is already in the packet, just set it to valid
        if name in self._headers:
            self._headers[name].set_valid()
            return

        # Find header type and field list to verify provided header.
        header_type = self._header_defs[name]["header_type"]
        header_fields = self._header_types[header_type]["fields"]

        # Create a zero header
        header = Header()
        for field in header_fields:
            field_name = field[0]
            field_bits = field[1]

            header[field_name] = BitVal(0, field_bits)

        self._headers[name] = header

    def is_valid(self, name):
        """Check if particular header is valid."""
        hdr = self._headers.get(name)
        return hdr.valid if hdr is not None else False

    def clear(self):
        """Clear the packet of all headers."""
        self._headers.clear()


class Bus:
    """The metadata + headers bus."""

    def __init__(self, standard_metadata, scalars, packet):
        """
        Parameters
        ----------
        standard_metadata : Header
            Architecture defined metadata
        scalars : Header
            Scalar variables
        packet : Packet
            The internal packet representation
        """
        self._standard_metadata = standard_metadata
        self._scalars = scalars
        self._packet = packet

    def clone(self):
        return deepcopy(self)

    @property
    def standard_metadata(self):
        return self._standard_metadata

    @property
    def packet(self):
        return self._packet

    def get_hdr(self, name):
        if name == "standard_metadata":
            return self._standard_metadata
        elif name == "scalars":
            return self._scalars
        else:
            return self._packet[name]
