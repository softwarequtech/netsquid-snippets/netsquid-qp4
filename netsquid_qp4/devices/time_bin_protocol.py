"""NetSquid protocol for injecting time binned packets."""

import abc

from netsquid.protocols import Protocol


class TimedProtocol(Protocol, metaclass=abc.ABCMeta):
    # NOTE: NetSquid has a TimedNodeProtocol, but that requires the owner of
    # the protocol to be a NetSquid Node object. This class is identical, but
    # it does not have that requirement.

    def __init__(self, period, start_time=0):
        super().__init__()
        self._period = period
        self._start_time = start_time

    def run(self):
        yield self.await_timer(self._start_time)
        while True:
            self.exec_protocol()
            yield self.await_timer(self._period)

    @abc.abstractmethod
    def exec_protocol(self):
        raise NotImplementedError


class TimeBinProtocol(TimedProtocol):

    def __init__(self, device, period):
        super().__init__(period)
        self._device = device

    def exec_protocol(self):
        self._device.new_time_bin()
