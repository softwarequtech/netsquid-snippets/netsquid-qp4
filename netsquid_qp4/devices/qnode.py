"""A P4 quantum node."""

from netsquid.components.component import Message
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
from netsquid_physlayer.entanglementGenerator import ExcitedPairPreparation

from netsquid_qp4.devices.switch import SwitchRuntime, QSwitch
from netsquid_qp4.packet import RawPacket


class QNode(QSwitch, Node):

    def __init__(self, name, bmv2_json, nqubits=1, nports=1):
        QSwitch.__init__(self, name, bmv2_json, nports, QNodeRuntime(self))

        qproc = QuantumProcessor(name=name, num_positions=nqubits)
        Node.__init__(self, name, qmemory=qproc)

        # Create outgoing quantum ports
        # 0x100 + 1+: quantum ports going to a mid-point station
        self.add_ports([str(0x100 + p) for p in range(1, nports+1)])

    def new_time_bin(self):
        hdr = self.switch.model.header("local")

        message = RawPacket()
        message.push(hdr)
        self.switch.ports["0"].tx_input(
            Message(items=[message], header=self.switch.next_header())
        )


class QNodeRuntime(SwitchRuntime):

    def __init__(self, node):
        super().__init__()
        self._node = node
        self._generator = ExcitedPairPreparation()
        self._alpha = 0.3

    def generate(self, port):
        spin, photon = self._generator.generate(self._alpha)

        self._node.qmemory.put(spin, 0)
        self._node.ports[str(0x100 + port)].tx_output(photon)
