"""A switch running a QP4 pipeline."""

from abc import ABCMeta, abstractmethod
from functools import partial
import logging

from netsquid.components.component import Component, Message
import netsquid as ns

from netsquid_qp4.comn import log
from netsquid_qp4.device import Device
from netsquid_qp4.devices.time_bin_protocol import TimeBinProtocol
from netsquid_qp4.v1quantum import (
    V1QuantumArchitecture,
    V1QuantumPortMeta,
    V1QuantumRuntime,
)


class Switch(Component, Device):

    def __init__(
            self,
            name,
            bmv2_json,
            port_names=None,
            runtime=None,
    ):
        # Will call add_ports
        Component.__init__(
            self,
            name,
            properties=None,
            models=None,
            subcomponents=None,
            port_names=port_names,
        )

        runtime = runtime or SwitchRuntime()
        Device.__init__(self, V1QuantumArchitecture(bmv2_json, runtime))

        # Create private logger for switch
        self._logger = logging.getLogger(name)
        log.setup_logging(self._logger)

        # Start the QP4 interface ONLY on ports passed in
        port_names = port_names if port_names is not None else []
        self._interfaces = {}
        self.start_interfaces(port_names)

        # Message counter to correlate messages in logs
        self._msg_id = -1

    def add_ports(self, names):
        # We require all port names to be convertible to int (and back)
        # 0x000-0x0FF :: classical ports
        # 0x100-0x1FF :: reserved for quantum ports
        for name in names:
            assert name == str(int(name))
            assert int(name) == (int(name) & 0x0FF)
        return super().add_ports(names)

    def next_msg_id(self):
        self._msg_id += 1
        return self._msg_id

    def next_header(self):
        return "{:-<6}-{:0>3}".format(self.name, self.next_msg_id())

    def start_interfaces(self, port_names):
        for name in port_names:
            port = self.ports[name]
            port_meta = V1QuantumPortMeta(port_num=int(port.name))
            port.bind_input_handler(partial(self.process, port_meta))

    def process(self, port_meta, message):
        self._logger.info("RCV - port: {:>2} - msg[{}]: {}"
                          .format(
                              port_meta.port_num,
                              message.meta["header"],
                              message.items[0],
                          ))

        raw_packet = message.items[0]
        port_packet_out = self.arch.send(port_meta, raw_packet)

        for port_out_meta, packet_out in port_packet_out:
            message = Message(items=[packet_out], header=self.next_header())

            self.ports[str(port_out_meta.port_num)].tx_output(message)
            self._logger.info("SND - port: {:>2} - msg[{}]: {}"
                              .format(
                                  port_out_meta.port_num,
                                  message.meta["header"],
                                  message.items[0],
                              ))


class SwitchRuntime(V1QuantumRuntime):

    def __init__(self):
        super().__init__()
        self._start_time = ns.sim_time(ns.MICROSECOND)

    def time(self):
        return ns.sim_time(ns.MICROSECOND) - self._start_time


class QSwitch(Component, metaclass=ABCMeta):
    # QSwitch contains a Switch object as a subcomponent and has all its ports
    # connected to the outside world via ports of the same name. This makes it
    # a convenient superclass for devices which want a P4 switch subcomponent.

    def __init__(self, name, bmv2_json, nports=1, runtime=None):
        super().__init__(name=name, port_names=None)

        # 0:  local port for local pseudo-packets
        # 1+: classical network ports connected to end-nodes
        port_names = ["0"]
        for p in range(1, nports+1):
            port_names += [str(p)]

        # Add switch as subcomponent
        switch = Switch(name, bmv2_json, port_names, runtime)
        self.add_subcomponent(name=switch.name, component=switch)
        self._switch = switch

        # Add the ports to the mid-point as well and connect them up
        self.add_ports(port_names)
        for port in self.ports.values():
            port.forward_input(self.switch.ports[port.name])
            self.switch.ports[port.name].forward_output(port)

        # Start the timer
        self._timer = TimeBinProtocol(self, period=10_000)
        self._timer.start()

    @property
    def switch(self):
        return self._switch

    @abstractmethod
    def new_time_bin(self):
        raise NotImplementedError
