"""Connection classes."""

from netsquid.components.channel import Channel
from netsquid.components.qchannel import QuantumChannel
from netsquid.components.component import Component
from netsquid.components.models import DelayModel
from netsquid.nodes import DirectConnection


class BasicDelayModel(DelayModel):
    def __init__(self):
        super().__init__()
        self.properties["c"] = 3e5
        self.required_properties = ['length']

    def generate_delay(self, **kwargs):
        speed = self.properties["c"]
        return 1e9 * kwargs['length'] / speed


class ClassicalConnection(DirectConnection):
    def __init__(self, name, distance, delay_model=None):
        delay_model = delay_model or BasicDelayModel()

        channel_ab = Channel(
            name="channel_ab",
            length=distance,
            models={"delay_model": delay_model},
        )
        channel_ba = Channel(
            name="channel_ba",
            length=distance,
            models={"delay_model": delay_model},
        )

        super().__init__(
            name=name,
            channel_AtoB=channel_ab,
            channel_BtoA=channel_ba,
        )


class QuantumConnection(DirectConnection):
    def __init__(self, name, distance, delay_model):
        delay_model = delay_model or BasicDelayModel()

        channel_ab = QuantumChannel(
            name="channel_ab",
            length=distance,
            models={"delay_model": delay_model},
        )

        super().__init__(
            name=name,
            channel_AtoB=channel_ab,
            channel_BtoA=None,
        )


class CombinedConnection(Component):
    def __init__(self, name, distance, delay_model=None):
        super().__init__(
            name=name,
            subcomponents={
                "class": ClassicalConnection("class", distance, delay_model),
                "quant": QuantumConnection("quant", distance, delay_model),
            },
            port_names=["ACl", "BCl", "AQu", "BQu"],
        )

        # Classical connection is bi-directional
        self.ports["ACl"].forward_input(self.subcomponents["class"].ports["A"])
        self.ports["BCl"].forward_input(self.subcomponents["class"].ports["B"])

        self.subcomponents["class"].ports["A"].forward_output(
            self.ports["ACl"]
        )
        self.subcomponents["class"].ports["B"].forward_output(
            self.ports["BCl"]
        )

        # Quantum connection is one-directional
        self.ports["AQu"].forward_input(self.subcomponents["quant"].ports["A"])
        self.subcomponents["quant"].ports["B"].forward_output(
            self.ports["BQu"]
        )
