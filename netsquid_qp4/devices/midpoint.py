"""A P4 mid-point."""

from netsquid.components.component import Component, Message
from netsquid_physlayer.entanglementGenerator import HeraldingMidPoint

from netsquid_qp4.devices.switch import QSwitch
from netsquid_qp4.packet import BitVal, Header, RawPacket


class MidPoint(QSwitch):

    class BeamSplitter(HeraldingMidPoint, Component):

        def __init__(self, switch):
            Component.__init__(
                self,
                name="beam-splitter",
                port_names=["L", "R"],
            )
            HeraldingMidPoint.__init__(
                self,
                pdark=0,
                detection_eff=1,
                visibility=1,
            )

            self._switch = switch

            self.L.bind_input_handler(self._recv_L)
            self.R.bind_input_handler(self._recv_R)

            self._time_bin_used = False
            self._lq = None
            self._rq = None

            self.new_time_bin()

        def new_time_bin(self):
            if not self._time_bin_used:
                self._switch.send_outcome(0x0)

            self._time_bin_used = False
            self._reset()

        def _reset(self):
            self._lq = None
            self._rq = None

        def _recv_L(self, message):
            # pylint:disable=invalid-name
            assert len(message.items) == 1
            self._lq = message.items[0]
            self._recv()

        def _recv_R(self, message):
            # pylint:disable=invalid-name
            assert len(message.items) == 1
            self._rq = message.items[0]
            self._recv()

        def _recv(self):
            if (self._lq is not None) and (self._rq is not None):
                self._switch.send_outcome(self.measure(self._lq, self._rq))
                self._time_bin_used = True
                self._reset()

        @property
        def L(self):
            # pylint:disable=invalid-name
            return self.ports["L"]

        @property
        def R(self):
            # pylint:disable=invalid-name
            return self.ports["R"]

    def __init__(self, name, bmv2_json, nports=2):
        super().__init__(name, bmv2_json, nports=nports)

        # The mid-point also has quantum ports which we do not connect directly
        # to the QP4 program.
        # 0x100 + 1+: quantum ports connected to end-nodes
        self.add_ports([str(0x100 + p) for p in range(1, nports+1)])

        # Add the beam splitter
        self.add_subcomponent(
            name="beam-splitter",
            component=MidPoint.BeamSplitter(self),
        )

        # Start with port 0x101 connected to L and 0x102 to R
        if nports >= 1:
            self.connect_to_L(str(0x101))
        if nports >= 2:
            self.connect_to_R(str(0x102))

    @property
    def beam_splitter(self):
        return self.subcomponents["beam-splitter"]

    def new_time_bin(self):
        self.beam_splitter.new_time_bin()

    def connect_to_L(self, port_name):
        # pylint:disable=invalid-name
        port = self.ports[port_name]
        port.forward_input(self.beam_splitter.L)

    def connect_to_R(self, port_name):
        # pylint:disable=invalid-name
        port = self.ports[port_name]
        port.forward_input(self.beam_splitter.R)

    def send_outcome(self, outcome):
        hdr = Header()
        hdr["detector"] = BitVal(outcome, 2)
        hdr["_unused"] = BitVal(0, 6)

        message = RawPacket()
        message.push(hdr)
        self.switch.ports["0"].tx_input(
            Message(items=[message], header=self.switch.next_header())
        )
