PYTHON3      = python3
SOURCEDIR    = netsquid_qp4
TESTDIR      = tests
COVREP       = term
PIP_FLAGS    = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org
MINCOV       = 0

help:
	@echo "install           Installs the package (editable)."
	@echo "verify            Verifies the installation, runs the linter and tests."
	@echo "tests             Runs the tests."
	@echo "open-cov-report   Creates and opens the coverage report."
	@echo "lint              Runs the flake8 linter."
	@echo "pylint            Runs the pylint linter."
	@echo "deploy-bdist      Builds and uploads the package to the netsquid pypi server."
	@echo "bdist             Builds the package."
	@echo "test-deps         Installs the requirements needed for running tests and linter."
	@echo "python-deps       Installs the requirements needed for using the package."
	@echo "ci-venv           Create virtualenv for CI job if one does not exist yet."
	@echo "clean             Removes all .pyc files."

test-deps:
	@$(PYTHON3) -m pip install --upgrade -r test_requirements.txt

python-deps: _check_variables
	@$(PYTHON3) -m pip install --upgrade -r requirements.txt ${PIP_FLAGS}

ci-venv: _check_venv_variables
	@if [ ! -d ${CI_VENV} ]; then $(PYTHON3) -m virtualenv ${CI_VENV}; fi

clean:
	@/usr/bin/find . -name '*.pyc' -delete
	@/usr/bin/rm -f .coverage
	@/usr/bin/find . -name __pycache__ -prune -exec rm -rf "{}" \;
	@/usr/bin/rm -rf htmlcov

pylint:
	@$(PYTHON3) -m pylint ${SOURCEDIR} ${TESTDIR}

lint:
	@$(PYTHON3) -m flake8 ${SOURCEDIR} ${TESTDIR}

tests:
	@$(PYTHON3) -m pytest -v --cov=${SOURCEDIR} --cov-report=${COVREP} tests

open-cov-report:
	xdg-open htmlcov/index.html

examples:
	@$(PYTHON3) -m examples.run_examples > /dev/null && echo "Examples OK!" || echo "Examples failed!"

_check_variables:
ifndef NETSQUIDPYPI_USER
	$(error Set the environment variable NETSQUIDPYPI_USER before uploading)
endif
ifndef NETSQUIDPYPI_PWD
	$(error Set the environment variable NETSQUIDPYPI_PWD before uploading)
endif

_check_venv_variables:
ifndef CI_VENV
	$(error Set the environment variable CI_VENV before creating virtualenv)
endif

verify: clean test-deps python-deps lint tests _verified

_verified:
	@echo "The snippet is verified :)"

# TODO: Leave commented while under development

# bdist:
# 	@$(PYTHON3) setup.py bdist_wheel

# install: _check_variables test-deps
# 	@$(PYTHON3) -m pip install -e . ${PIP_FLAGS}

# _clean_dist:
# 	@/bin/rm -rf dist

# deploy-bdist: _clean_dist bdist
# 	@$(PYTHON3) setup.py deploy

.PHONY: clean ci-venv lint test-deps python-deps tests verify bdist deploy-bdist _clean_dist install open-cov-report _check_variables examples
