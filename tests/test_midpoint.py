"""Unit tests for NetSquid MidPoint class."""

from functools import partial

from devices.connections import CombinedConnection
from devices.midpoint import MidPoint
from packet import BitVal

from . import util


def test_midpoint(ns):
    # Create the midpoint
    midpoint = MidPoint(
        name="mp",
        bmv2_json="tests/p4/midpoint.json",
        nports=2,
    )

    # Create the connections
    left_conn = CombinedConnection(name="left_conn", distance=3e-3)
    right_conn = CombinedConnection(name="right_conn", distance=3e-3)

    # Connect the pieces
    midpoint.ports[str(0x001)].connect(left_conn.ports["BCl"])
    midpoint.ports[str(0x101)].connect(left_conn.ports["BQu"])

    midpoint.ports[str(0x002)].connect(right_conn.ports["BCl"])
    midpoint.ports[str(0x102)].connect(right_conn.ports["BQu"])

    # We first bind assert handlers to make sure the midpoint isn't sending
    # anything to start with
    left_conn.ports["ACl"].bind_output_handler(util.assert_handler)
    right_conn.ports["ACl"].bind_output_handler(util.assert_handler)

    # MidPoint runs with a period of 10_000
    ns.sim_run(duration=100_000)

    # Setup a received value object to make sure we actually receive somthing
    received_port = BitVal(0, 2)

    # Now bind an actual handler that simply checks it receives a message with
    # a detector result that matches the port
    def detector_handler(message, port_num):
        assert len(message.items) == 1

        packet = message.items[0]
        assert len(packet) == 1

        header = packet.pop()
        assert header["detector"] == BitVal(port_num, 2)
        received_port.val = port_num

        # Immediately rebind assert handlers to make sure only one message is
        # received and no more
        left_conn.ports["ACl"].bind_output_handler(util.assert_handler)
        right_conn.ports["ACl"].bind_output_handler(util.assert_handler)

    left_conn.ports["ACl"].bind_output_handler(
        partial(detector_handler, port_num=1)
    )
    right_conn.ports["ACl"].bind_output_handler(
        partial(detector_handler, port_num=2)
    )

    # Inject qubits
    qubits = ns.qubits.create_qubits(2)
    ns.qubits.operate(qubits[1], ns.X)
    left_conn.ports["AQu"].tx_input(qubits[0])
    right_conn.ports["AQu"].tx_input(qubits[1])

    # Run again
    ns.sim_run(duration=100_000)

    # Check we received something
    assert received_port.val != 0
