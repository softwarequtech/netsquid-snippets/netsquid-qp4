"""Unit tests for P4 expression evaluation."""

from packet import BitVal, Header, RawPacket

from mock_device import MockDevice


def check_operator(op, in32a=None, in32b=None, in8=None, in1=None,
                   out32=None, out1=None):

    header = Header()
    # op-code encoding is in tests/p4/expression.p4 table operations
    header["op"] = BitVal(op, 8)
    header["in32a"] = BitVal(in32a or 0, 32)
    header["in32b"] = BitVal(in32b or 0, 32)
    header["in8"] = BitVal(in8 or 0, 8)
    header["in1"] = BitVal(in1 or 0, 1)
    header["out32"] = BitVal(out32 or 0, 32)
    header["out1"] = BitVal(out1 or 0, 1)
    header["padding"] = BitVal(0, 6)
    packet_in = RawPacket()
    packet_in.push(header)

    switch = MockDevice("tests/p4/expressions.json")
    port_in = 0

    port_out, packet_out = switch.send(port_in, packet_in)[0]

    assert port_out is not None
    assert packet_out is not None
    assert port_out == 0
    header = packet_out.pop()
    if out32 is not None:
        assert header["out32"] == BitVal(out32, 32)
    if out1 is not None:
        assert header["out1"] == BitVal(out1, 1)


def test_add():
    # 123 + 456 = 579
    check_operator(op=1, in32a=123, in32b=456, out32=579)


def test_subtract():
    # 789 - 456 = 333
    check_operator(op=2, in32a=789, in32b=456, out32=333)


def test_multiply():
    # 762 * 976 = 743712
    check_operator(op=3, in32a=762, in32b=976, out32=743712)


def test_left_shift():
    # 0b101 << 3 = 0b101000
    check_operator(op=4, in32a=0b101, in8=3, out32=0b101000)


def test_right_shift():
    # 0b101011 >> 3 = 0b101
    check_operator(op=5, in32a=0b101011, in8=3, out32=0b101)


def test_is_equal():
    # 123 == 123: true
    check_operator(op=6, in32a=123, in32b=123, out1=1)
    # 123 == 456: false
    check_operator(op=6, in32a=123, in32b=456, out1=0)


def test_is_not_equal():
    # 123 != 123: false
    check_operator(op=7, in32a=123, in32b=123, out1=0)
    # 123 != 456: true
    check_operator(op=7, in32a=123, in32b=456, out1=1)


def test_greater_than():
    # 88888 > 77777: true
    check_operator(op=8, in32a=88888, in32b=77777, out1=1)
    # 88888 > 88888: false
    check_operator(op=8, in32a=88888, in32b=88888, out1=0)
    # 88888 > 99999: false
    check_operator(op=8, in32a=88888, in32b=99999, out1=0)


def test_greater_than_or_equal():
    # 88888 >= 77777: true
    check_operator(op=9, in32a=88888, in32b=77777, out1=1)
    # 88888 >= 88888: true
    check_operator(op=9, in32a=88888, in32b=88888, out1=1)
    # 88888 >= 99999: false
    check_operator(op=9, in32a=88888, in32b=99999, out1=0)


def test_less_than():
    # 88888 < 77777: false
    check_operator(op=10, in32a=88888, in32b=77777, out1=0)
    # 88888 < 88888: false
    check_operator(op=10, in32a=88888, in32b=88888, out1=0)
    # 88888 < 99999: true
    check_operator(op=10, in32a=88888, in32b=99999, out1=1)


def test_less_than_or_equal():
    # 88888 <= 77777: false
    check_operator(op=11, in32a=88888, in32b=77777, out1=0)
    # 88888 <= 88888: true
    check_operator(op=11, in32a=88888, in32b=88888, out1=1)
    # 88888 <= 99999: true
    check_operator(op=11, in32a=88888, in32b=99999, out1=1)


def test_logical_and():
    # Note: in32a<10: true, in32b>=10: false, ditto for in32b
    # false and false: false
    check_operator(op=12, in32a=11, in32b=11, out1=0)
    # false and true: false
    check_operator(op=12, in32a=11, in32b=1, out1=0)
    # true and false: false
    check_operator(op=12, in32a=1, in32b=11, out1=0)
    # true and true: true
    check_operator(op=12, in32a=1, in32b=1, out1=1)


def test_logical_or():
    # Note: in32a<10: true, in32b>=10: false, ditto for in32b
    # false or false: false
    check_operator(op=13, in32a=11, in32b=11, out1=0)
    # false or true: true
    check_operator(op=13, in32a=11, in32b=1, out1=1)
    # true or false: true
    check_operator(op=13, in32a=1, in32b=11, out1=1)
    # true or true: true
    check_operator(op=13, in32a=1, in32b=1, out1=1)


def test_logical_not():
    # Note: in32a<10: true, in32b>=10: false
    # not false: true
    check_operator(op=14, in32a=11, out1=1)
    # not true: false
    check_operator(op=14, in32a=1, out1=0)


def test_bitwise_and():
    # 110 & 011: 010
    check_operator(op=15, in32a=0b110, in32b=0b011, out32=0b010)


def test_bitwise_or():
    # 110 | 011: 111
    check_operator(op=16, in32a=0b110, in32b=0b011, out32=0b111)


def test_bitwise_xor():
    # 110 ^ 011: 101
    check_operator(op=17, in32a=0b110, in32b=0b011, out32=0b101)


def test_bitwise_not():
    # ~ 110: 011
    check_operator(op=18, in32a=0b00000000000000000000000000000110,
                   out32=0b11111111111111111111111111111001)


def test_is_valid():
    # TODO: operator 19
    pass


def test_is_valid_union():
    # TODO: operator 20
    pass


def test_data_to_bool():
    # Out1 is set to true if the header is valid (i.e. always)
    check_operator(op=21, out1=1)


def test_bool_to_data():
    # false -> 0
    check_operator(op=22, in1=0, out1=0)
    # true -> 1
    check_operator(op=22, in1=1, out1=1)


def test_expr_bool():
    # this test is here to just hit an artificial expression with a raw boolean - it always
    # evaluates to true and returns 1 in out1.
    check_operator(op=23, in1=0, out1=1)
    check_operator(op=23, in1=1, out1=1)


def test_act_two_comp_mod():
    # TODO: operator 24
    pass


def test_act_sat_cast():
    # TODO: operator 25
    pass


def test_act_usat_cast():
    # TODO: operator 26
    pass


def test_act_ternary():
    # TODO: operator 27
    pass


def test_act_deref_header_stack():
    # TODO: operator 28
    pass


def test_act_last_stack_index():
    # TODO: operator 29
    pass


def test_act_size_stack():
    # TODO: operator 30
    pass


def test_act_access_field():
    # TODO: operator 31
    pass


def test_act_dereference_union_stack():
    # TODO: operator 32
    pass


def test_act_access_union_header():
    # TODO: operator 33
    pass
