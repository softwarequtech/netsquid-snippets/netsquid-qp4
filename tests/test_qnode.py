"""Unit tests for QNode class."""

from netsquid.qubits.qubit import Qubit

from devices.connections import CombinedConnection
from devices.qnode import QNode
from packet import BitVal


def test_qnode(ns):
    # Create the QNode
    qnode = QNode(
        name="qnode",
        bmv2_json="tests/p4/qnode.json",
        nqubits=1,
        nports=1,
    )

    # Create the connections
    conn = CombinedConnection(name="conn", distance=3e-3)

    # Connect the pieces
    qnode.ports[str(0x001)].connect(conn.ports["ACl"])
    qnode.ports[str(0x101)].connect(conn.ports["AQu"])

    def conn_cl_handler(message):
        assert len(message.items) == 1
        packet = message.items[0]

        assert len(packet) == 1
        header = packet.pop()

        assert header["port"] == BitVal(0x001, 8)
    conn.ports["BCl"].bind_output_handler(conn_cl_handler)

    photons = []

    def conn_qu_handler(message):
        assert len(message.items) == 1
        photon = message.items[0]
        assert isinstance(photon, Qubit)
        assert photon.qstate is not None

        photons.append(photon)
        if len(photons) > 1:
            # Ensure the new photon is not related to previous one
            assert photons[-1].qstate != photons[-2].qstate
    conn.ports["BQu"].bind_output_handler(conn_qu_handler)

    ns.sim_run(duration=100_000)
