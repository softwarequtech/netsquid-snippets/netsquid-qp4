"""Special pytest file for shared fixtures."""

import pytest


@pytest.fixture
def ns():
    # pylint: disable=import-outside-toplevel
    import netsquid
    netsquid.sim_reset()
    return netsquid
