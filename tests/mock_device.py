"""Mock device for tests that should avoid a simulation runtime."""

import time

from device import Device
from v1quantum import (
    V1QuantumArchitecture,
    V1QuantumRuntime,
    V1QuantumPortMeta,
)


class MockDevice(Device):

    def __init__(self, bm_json):
        super().__init__(V1QuantumArchitecture(bm_json, MockRuntime()))

    def send(self, port_in, packet_in):
        port_in_meta = V1QuantumPortMeta(port_num=port_in)
        arch_port_packet_out = self.arch.send(port_in_meta, packet_in)

        port_packet_out = []
        for port_out_meta, packet_out in arch_port_packet_out:
            port_packet_out.append((port_out_meta.port_num, packet_out))

        return port_packet_out


class MockRuntime(V1QuantumRuntime):

    def __init__(self):
        super().__init__()
        self._start_time = int(time.time() * 1_000_000)

    def time(self):
        return int(time.time() * 1_000_000) - self._start_time
