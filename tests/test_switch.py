"""Unit tests for NetSquid Switch class."""

from devices.connections import ClassicalConnection
from devices.switch import Switch
from packet import BitVal, RawPacket

from . import util


def test_ping_pong(ns):
    # Create switch
    switch = Switch(
        name="sw",
        bmv2_json="tests/p4/ping-pong.json",
        port_names=["0"],
    )

    # Every message should take 1000 ns to propagate
    connection = ClassicalConnection(name="connection", distance=3e-1)

    # Connect to the switch
    connection.ports["B"].connect(switch.ports["0"])

    # Create ping pong packet
    ttl = BitVal(100, 8)
    hdr = switch.model.header("ping_pong")
    hdr["ttl"] = ttl

    # Extract the other fields from the default
    ping_count = hdr["ping_count"]
    last_time = hdr["last_time"]
    cur_time = hdr["cur_time"]

    ping = RawPacket()
    ping.push(hdr)

    # Start by sending the ping packet to the switch
    connection.ports["A"].tx_input(ping)

    # To verify the responses we bind an output handler to port A
    def pong_handler(message):
        assert len(message.items) == 1
        pong = message.items[0]

        assert len(pong) == 1
        header = pong.pop()

        ttl.val -= 1
        assert "ttl" in header
        assert header["ttl"] == ttl

        ping_count.val += 1
        assert "ping_count" in header
        assert header["ping_count"] == ping_count

        last_time.val = cur_time.val
        assert "last_time" in header
        assert header["last_time"] == last_time

        # The fibre length was chosen such that a one-way trip is exactly 1
        # microsecond in simulated time
        cur_time.val += 2 if cur_time.val != 0 else 1
        assert "cur_time" in header
        assert header["cur_time"] == cur_time

        ping = RawPacket()
        ping.push(hdr)
        connection.ports["A"].tx_input(ping)

    connection.ports["A"].bind_output_handler(pong_handler)

    # Run for 200_000 ns, i.e. a full 100xRTT
    ns.sim_run(duration=200_000)

    # We will have sent the last message with TTL 1 and not received a response
    assert ttl.val == 1

    # Bind a new input handler that will assert if we receive another message
    connection.ports["A"].bind_output_handler(util.assert_handler)

    # Run for another 200_000 ns, i.e. a full 100xRTT to make sure that really
    # nothing is coming back
    ns.sim_run(duration=200_000)
