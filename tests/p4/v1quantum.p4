/* -*- P4_16 -*- */
#include <core.p4>
#include <v1qr.p4>

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/


header gen_t {
    bit<8> id;
}

struct metadata {
    /* empty */
}

struct headers {
    gen_t gen;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParser(
    packet_in packet,
    out headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {

    state start {
        packet.extract(hdr.gen);
        transition accept;
    }

}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}


/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    action drop() {
        mark_to_drop(standard_metadata);
    }

    action action_mp_gen_response(bit<9> port_1, bit<9> port_2) {
        mp_gen_response(standard_metadata, port_1, port_2);
    }

    table v1q_tbl {
        key = {
            hdr.gen.id: exact;
        }
        actions = {
            drop;
            action_mp_gen_response;
        }
        default_action = drop();
    }

    apply {
        if (hdr.gen.isValid()) {
            v1q_tbl.apply();
        }
    }
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    action drop() {
        mark_to_drop(standard_metadata);
    }

    action update_id() {
        hdr.gen.id = (bit<8>)standard_metadata.egress_spec;
    }

    table v1q_tbl {
        key = {
            standard_metadata.egress_spec: exact;
        }
        actions = {
            drop;
            update_id;
        }
        default_action = update_id();
    }

    apply {
        if (hdr.gen.isValid()) {
            v1q_tbl.apply();
        }
    }
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers  hdr, inout metadata meta) {
    apply { }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.gen);
    }
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
    MyParser(),
    MyVerifyChecksum(),
    MyIngress(),
    MyEgress(),
    MyComputeChecksum(),
    MyDeparser()
) main;
