/* -*- P4_16 -*- */

/*
 * complex.p4 : A P4 program that tries to exercise as many features of the
 * P4 language as possible. There is a separate program expr.p4 for
 * exercising all expressions.
 *
 * The P4 code is "inspired" by a traditional IP router, to make it easy  to
 * follow the code. That said, this is not intended to be a realistic or
 * feature complete or even correct IP router. This is just a test program
 * to exercise as many of the P4 language features as possible.
 */

#include <core.p4>
#include <v1qr.p4>

header ipv4_header_t {
    bit<32> dst_addr;
    bit<8> ttl;
}

// Header that's not meant to be used to make sure we can handle invalid headers.
header invalid_header_t {
    bit<8> _invalid;
}

struct metadata_t {
    bit<8> metadata_register;
}

struct headers_t {
    ipv4_header_t ipv4_header;
    invalid_header_t invalid_header;
}

parser PacketParser(
    packet_in packet,
    out headers_t headers,
    inout metadata_t metadata,
    inout standard_metadata_t standard_metadata
) {

    state start {
        transition ipv4;
    }

    state ipv4 {
        packet.extract(headers.ipv4_header);
        transition accept;        
    }

}

control PacketVerifyChecksum(inout headers_t headers, inout metadata_t metadata) {
    apply {  }
}

control ProcessIngressIPv4(
    inout headers_t headers,
    inout metadata_t metadata,
    inout standard_metadata_t standard_metadata
) {
    action act_miss() {
        mark_to_drop(standard_metadata);
    }

    action act_hit(bit<9> out_port) {
        standard_metadata.egress_spec = out_port;
    }

    table ipv4_fib {
        key = {
            headers.ipv4_header.dst_addr: lpm;
        }
        actions = {
            act_miss;
            act_hit;
        }
        const default_action = act_miss();
        const entries = {
            32w0x0a010203 &&& 32w0xffffffff: act_hit(1);   // 10.1.2.3/32 -> 1
            32w0x0a010200 &&& 32w0xffffff00: act_hit(2);   // 10.1.2.0/24 -> 2
            32w0x0a010000 &&& 32w0xffff0000: act_hit(3);   // 10.1.0.0/16 -> 3
            32w0x0a000000 &&& 32w0xff000000: act_hit(4);   // 10.0.0.0/8 -> 4
        }
    }

    apply {
        if (headers.ipv4_header.isValid()) {
            // Store TTL in the metadata to make sure the code is capable of using metadata.
            metadata.metadata_register = headers.ipv4_header.ttl;
            ipv4_fib.apply();
        }
        if (headers.invalid_header.isValid()) {
            headers.invalid_header._invalid = 1;
        }
    }
}

control ProcessIngress(
    inout headers_t headers,
    inout metadata_t metadata,
    inout standard_metadata_t standard_metadata
) {
    ProcessIngressIPv4() process_ingress_ipv4; 
    apply {
        process_ingress_ipv4.apply(headers, metadata, standard_metadata);
    }   
}

control ProgressEgress(
    inout headers_t headers,
    inout metadata_t metadata,
    inout standard_metadata_t standard_metadata
) {
    apply {
        if (headers.ipv4_header.isValid()) {
            // Decrement the TTL by using the value we stored in the metadata.
            headers.ipv4_header.ttl = metadata.metadata_register - 1;
        }
    }
}

control PacketComputeChecksum(inout headers_t headers, inout metadata_t metadata) {
    apply { }
}

control PacketDeparser(packet_out packet, in headers_t headers) {
    apply {
        packet.emit(headers.ipv4_header);
    }
}

V1Switch(
    PacketParser(),
    PacketVerifyChecksum(),
    ProcessIngress(),
    ProgressEgress(),
    PacketComputeChecksum(),
    PacketDeparser()
) main;
