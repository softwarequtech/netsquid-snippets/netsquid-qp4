"""Unit tests for complex P4 program that uses many P4 features."""

from mock_device import MockDevice
from packet import BitVal, RawPacket


def test_one_packet():

    switch = MockDevice("tests/p4/complex.json")

    header = switch.model.header("ipv4_header")
    header["dst_addr"] = BitVal(0x0a010101, 32)     # 10.1.1.1
    header["ttl"] = BitVal(32, 8)

    packet_in = RawPacket()
    packet_in.push(header)
    port_in = 0

    port_out, packet_out = switch.send(port_in, packet_in)[0]

    assert port_out == 3
    assert packet_out is not None
    ipv4_header = packet_out.pop()
    assert ipv4_header["ttl"] == BitVal(31, 8)
