"""Unit tests for V1Quantum architecture features."""

from packet import RawPacket
from mock_device import MockDevice


def test_registers():
    switch = MockDevice("tests/p4/ping-pong.json")

    # Create two ping packets
    header = switch.model.header("ping_pong")
    header["ttl"].val = 100
    ping_0 = RawPacket()
    ping_0.push(header)

    header = switch.model.header("ping_pong")
    header["ttl"].val = 100
    ping_1 = RawPacket()
    ping_1.push(header)

    # Verify mainline count behaviour
    port_in = 0
    port_out, ping_0 = switch.send(port_in, ping_0)[0]
    assert port_out == port_in
    header = ping_0.pop()
    assert header["ping_count"].val == 1

    ping_0.push(header)
    for _ in range(5):
        port_out, ping_0 = switch.send(port_in, ping_0)[0]
        assert port_out == port_in

    header = ping_0.pop()
    assert header["ping_count"].val == 6
    ping_0.push(header)

    # Verify registers are per-port as specified in the P4 program
    port_in = 3
    for _ in range(4):
        port_out, ping_1 = switch.send(port_in, ping_1)[0]
        assert port_out == port_in

    header = ping_1.pop()
    assert header["ping_count"].val == 4
    ping_1.push(header)

    # Verify the count uses the port's history, not the packet's history
    port_in = 2
    port_out, ping_0 = switch.send(port_in, ping_0)[0]
    assert port_out == port_in

    header = ping_0.pop()
    assert header["ping_count"].val == 1
    ping_0.push(header)


def test_mp_gen_response_extern():
    switch = MockDevice("tests/p4/v1quantum.json")

    # Insert a table entry that will apply the mp_gen_response extern.
    switch.arch.ingress_tables["MyIngress.v1q_tbl"].insert_entry(
        key=0,
        action_name="MyIngress.action_mp_gen_response",
        action_data=[1, 2],
    )

    # Insert a second table entry that will also apply the mp_gen_response extern, but with
    # different output ports.
    switch.arch.ingress_tables["MyIngress.v1q_tbl"].insert_entry(
        key=1,
        action_name="MyIngress.action_mp_gen_response",
        action_data=[3, 4],
    )

    # Create the input packet
    header = switch.model.header("gen")
    header["id"].val = 0
    packet_in = RawPacket()
    packet_in.push(header)

    # Sent in, this should result in two output packets.
    packets = switch.send(0, packet_in)

    # Check the packet contents
    expected_ports = set([1, 2])
    for port_out, packet in packets:
        header = packet.pop()
        assert port_out == header["id"].val
        assert port_out in expected_ports
        expected_ports.remove(port_out)
    assert not expected_ports

    # Create the second input packet to match the other entry
    header = switch.model.header("gen")
    header["id"].val = 1
    packet_in = RawPacket()
    packet_in.push(header)

    # Sent in, this should result in two output packets.
    packets = switch.send(0, packet_in)

    # Check the packet contents
    expected_ports = set([3, 4])
    for port_out, packet in packets:
        header = packet.pop()
        assert port_out == header["id"].val
        assert port_out in expected_ports
        expected_ports.remove(port_out)
    assert not expected_ports


def test_egress_drop():
    switch = MockDevice("tests/p4/v1quantum.json")

    # Insert a table entry that will apply the mp_gen_response extern.
    switch.arch.ingress_tables["MyIngress.v1q_tbl"].insert_entry(
        key=0,
        action_name="MyIngress.action_mp_gen_response",
        action_data=[1, 2],
    )

    # Create the input packet
    header = switch.model.header("gen")
    header["id"].val = 0
    packet_in = RawPacket()
    packet_in.push(header)

    # Insert another table entry, but at egress to drop that packet.
    switch.arch.egress_tables["MyEgress.v1q_tbl"].insert_entry(
        key=1,
        action_name="MyEgress.drop",
        action_data=[],
    )

    # Sent in, this should result in only one packet now on port 2.
    packets = switch.send(1, packet_in)

    # Check the packet contents
    expected_ports = set([2])
    for port_out, packet in packets:
        header = packet.pop()
        assert port_out == header["id"].val
        assert port_out in expected_ports
        expected_ports.remove(port_out)
    assert not expected_ports
