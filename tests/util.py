"""Utilities for unit tests."""


def assert_handler(message):
    """Handler that asserts if it receives a message."""
    assert message is None
