"""Unit tests for P4 ping-pong program."""

from packet import BitVal, Header, RawPacket

from mock_device import MockDevice


def test_ttl_100():

    header = Header()
    header["ttl"] = BitVal(100, 8)
    header["ping_count"] = BitVal(0, 32)
    header["last_time"] = BitVal(0, 48)
    header["cur_time"] = BitVal(0, 48)

    packet_in = RawPacket()
    packet_in.push(header)

    switch = MockDevice("tests/p4/ping-pong.json")
    port_in = 0

    port_out, packet_out = switch.send(port_in, packet_in)[0]

    assert port_out == 0
    assert packet_out is not None
    header = packet_out.pop()
    assert header["ttl"] == BitVal(99, 8)
