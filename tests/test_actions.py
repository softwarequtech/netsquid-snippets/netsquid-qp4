"""Unit test non-extern actions."""

from packet import RawPacket

from mock_device import MockDevice


def test_valid_invalid():
    switch = MockDevice("tests/p4/actions.json")
    port_in = 0

    hdr = switch.model.header("act")

    packet_in = RawPacket()
    packet_in.push(hdr)

    # Action 0 will call set valid on the test header
    hdr["action_id"].val = 0
    _, packet_out = switch.send(port_in, packet_in)[0]

    assert len(packet_out) == 2
    act_hdr = packet_out.pop()
    test_hdr = packet_out.pop()
    assert test_hdr["value"].val == 0xaa  # As defined in P4 program

    # Action 1 will call set invalid on the test header and then valid again
    act_hdr["action_id"].val = 1

    packet_in = RawPacket()
    packet_in.push(test_hdr)
    packet_in.push(act_hdr)

    _, packet_out = switch.send(port_in, packet_in)[0]

    assert len(packet_out) == 2
    act_hdr = packet_out.pop()
    test_hdr = packet_out.pop()
    assert test_hdr["value"].val == 0xbb  # As defined in P4 program

    # Action 1 will call set invalid on the test header and then valid again
    act_hdr["action_id"].val = 2

    packet_in = RawPacket()
    packet_in.push(test_hdr)
    packet_in.push(act_hdr)

    _, packet_out = switch.send(port_in, packet_in)[0]

    assert len(packet_out) == 1
    act_hdr = packet_out.pop()
    assert act_hdr["action_id"].val == 2
