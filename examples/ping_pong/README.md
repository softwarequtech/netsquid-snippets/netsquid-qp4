# Ping Pong example

This example illustrates the basic soft switch.

## Topology

```
 ----                         ----
|    |                       |    |
| R0 | ----- Classical ----- | R1 |
|    |                       |    |
 ----                         ----
```

The two switches both use port `0x0` for the connection.

## Instructions

Run this example from the top-level directory:
```
python3 -m examples.ping_pong.run
```

This will load the program from [`p4/ping-pong.json`](p4/ping-pong.json) which
was compiled from [`p4/ping-pong.p4`](p4/ping-pong.p4).

## What you will see

All output is available in `logs/R0.log` and `logs/R1.log` (relative to
repository root).

A packet with a TTL header initialised to 100 is sent from `R0` to `R1`. The
switches will keep decrementing the TTL and returning the packet until the TTL
is decremented to 0.
