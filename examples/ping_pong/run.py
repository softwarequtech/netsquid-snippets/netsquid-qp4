import netsquid as ns

from netsquid_qp4.comn import typemap
from netsquid_qp4.comn.netconf import NetConf
from netsquid_qp4.packet import BitVal, RawPacket


def main():
    typemap.setup_builder_type_map()
    config = NetConf(config_file="./examples/ping_pong/config.yml")

    node_ping = config.components["R0"]

    hdr = node_ping.model.header("ping_pong")
    hdr["ttl"] = BitVal(100, 8)

    ping = RawPacket()
    ping.push(hdr)

    node_ping.ports["0"].tx_output(ping)

    ns.sim_run(duration=20_000)


if __name__ == "__main__":
    main()
