# Midpoint example

This example illustrates the midpoint soft switch.

## Topology

```
                     --------------
     Classical -----|0x001    0x002|----- Classical
LEFT                |      MP      |                RIGHT
     Quantum   -----|0x101    0x102|----- Quantum
                     --------------
```

The midpoint is connected to its detectors on port `0x000`.

## Instructions

Run this example from the top-level directory:
```
python3 -m examples.midpoint.run
```

This will load the program from [`p4/midpoint.json`](p4/midpoint.json) which
was compiled from [`p4/midpoint.p4`](p4/midpoint.p4).

## What you will see

All output is available in `logs/MP.log` (relative to repository root).

The midpoint receives a reading in every time bin (set to 10,000 ns) from the
detector by means of a pseudo-packet sent over port `0x000`. If no qubits
arrive in the time bin, the detector won't click and the reading will be zero.

The example script will send in a qubit from either side of the midpoint in the
same time window. The midpoint's detector will click and send a pseudo-packet
over port `0x000` with a readout of either `1` or `2`. The P4 program then
forwards this packet to the port whose name corresponds to the detector's
readout.

Note that this is a physically meaningless example. It only serves to
illustrate a simple P4 program that receives a quantum measurement readout from
the hardware and sends out packet in response.
