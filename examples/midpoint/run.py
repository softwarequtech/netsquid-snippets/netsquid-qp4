import netsquid as ns

from netsquid_qp4.comn import typemap
from netsquid_qp4.comn.netconf import NetConf


def main():
    typemap.setup_builder_type_map()
    config = NetConf(config_file="./examples/midpoint/config.yml")

    left_conn = config.components["left_conn"]
    right_conn = config.components["right_conn"]

    left_conn.ports["ACl"].bind_output_handler(
        lambda message: print(
            "Received {} on LEFT connection".format(message.items[0])
        )
    )
    right_conn.ports["ACl"].bind_output_handler(
        lambda message: print(
            "Received {} on RIGHT connection".format(message.items[0])
        )
    )

    ns.sim_run(duration=100_000)

    qubits = ns.qubits.create_qubits(2)
    ns.qubits.operate(qubits[1], ns.X)
    left_conn.ports["AQu"].tx_input(qubits[0])
    right_conn.ports["AQu"].tx_input(qubits[1])

    ns.sim_run(duration=100_000)


if __name__ == "__main__":
    main()
