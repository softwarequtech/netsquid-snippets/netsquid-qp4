import netsquid as ns

from netsquid_qp4.comn import typemap
from netsquid_qp4.comn.netconf import NetConf


def main():
    typemap.setup_builder_type_map()
    config = NetConf(config_file="./examples/mhp/config.yml")

    # Populate table with times and attempt parameters
    ra = config.components["RA"]
    rb = config.components["RB"]

    for cycle in range(5):
        ra.switch.arch.ingress_tables["MyIngress.gen_tbl"].insert_entry(
            key=cycle,
            action_name="MyIngress.gen",
            action_data=[1, 10, 20],
        )
        rb.switch.arch.ingress_tables["MyIngress.gen_tbl"].insert_entry(
            key=cycle,
            action_name="MyIngress.gen",
            action_data=[1, 10, 20],
        )

    for cycle in range(5, 10):
        ra.switch.arch.ingress_tables["MyIngress.gen_tbl"].insert_entry(
            key=cycle,
            action_name="MyIngress.gen",
            action_data=[1, 4, 20],
        )
        rb.switch.arch.ingress_tables["MyIngress.gen_tbl"].insert_entry(
            key=cycle,
            action_name="MyIngress.gen",
            action_data=[1, 8, 60],
        )

    ns.sim_run(duration=100_000)


if __name__ == "__main__":
    main()
