/* -*- P4_16 -*- */
#include <core.p4>
#include <v1qr.p4>

typedef bit<48> time_t;

const bit<9> LOCAL = 0x00;
const time_t TIME_TOL = 2;

const bit<4> QUEUE_MISMATCH = 0x04;

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/


header local_t {
    bit<2> detector;
    bit<6> _unused;
}

header gen_t {
    bit<12> _header;
    bit<4> qid;
    bit<8> qseq;
    bit<8> _unused;
}

header mp_reply_t {
    bit<12> _header;
    bit<4> outcome;
    bit<16> seq;
    bit<4> qid;
    bit<8> qseq;
    bit<4> qidp;
    bit<8> qseqp;
    bit<8> _unused;
}

struct metadata {
    /* empty */
}

struct headers {
    local_t local;
    gen_t gen;
    mp_reply_t mp_reply;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParser(
    packet_in packet,
    out headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {

    state start {
        transition select(standard_metadata.ingress_port) {
            LOCAL:   parse_local;
            default: parse_gen;
        }
    }

    state parse_local {
        packet.extract(hdr.local);
        transition accept;
    }

    state parse_gen {
        packet.extract(hdr.gen);
        transition accept;
    }

}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}


/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

register<bit<4>>(1) r_result;
register<time_t>(1) r_result_time;

// Three slots so that we can index using 1 and 2
register<bit<4>>(3) r_qid;
register<bit<8>>(3) r_qseq;
register<time_t>(3) r_gen_time;

register<bit<16>>(1) r_seq;
register<time_t>(1) r_last_sent;

control MyIngress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    time_t current_time = 0;
    time_t time_1 = 0;
    time_t time_2 = 0;

    bit<9> rsp_port_1 = 0;
    bit<9> rsp_port_2 = 0;

    action drop() {
        mark_to_drop(standard_metadata);
    }

    action record_local(bit<9> port_1, bit<9> port_2) {
        time_t detector_time = standard_metadata.ingress_global_timestamp;

        r_result.write(0, (bit<4>)hdr.local.detector);
        r_result_time.write(0, detector_time);

        // Compare time with the gen packets and call gen_response which will
        // multicast response packets to the indicated output ports.
        time_t gen_time_1;
        time_t gen_time_2;
        r_gen_time.read(gen_time_1, (bit<32>)port_1);
        r_gen_time.read(gen_time_2, (bit<32>)port_2);

        current_time = detector_time;
        time_1 = gen_time_1;
        time_2 = gen_time_2;

        rsp_port_1 = port_1;
        rsp_port_2 = port_2;
    }

    action record_gen(bit<9> other_port) {
        bit<9> port = standard_metadata.ingress_port;
        time_t gen_time = standard_metadata.ingress_global_timestamp;

        r_qid.write((bit<32>)port, hdr.gen.qid);
        r_qseq.write((bit<32>)port, hdr.gen.qseq);
        r_gen_time.write((bit<32>)port, gen_time);

        // Compare time with the other gen packet and the detector time and
        // call gen_response which will multicast response packets to the
        // indicated output ports.
        time_t gen_time_other;
        time_t detector_time;
        r_gen_time.read(gen_time_other, (bit<32>)other_port);
        r_result_time.read(detector_time, 0);

        current_time = gen_time;
        time_1 = gen_time_other;
        time_2 = detector_time;

        rsp_port_1 = port;
        rsp_port_2 = other_port;
    }

    table mp_tbl {
        key ={
            standard_metadata.ingress_port: exact;
        }
        actions = {
            record_gen;
        }
        const entries= {
            1: record_gen(2);
            2: record_gen(1);
        }
    }

    apply {
        if (hdr.local.isValid()) {
            record_local(1, 2);
        } else if (hdr.gen.isValid()) {
            mp_tbl.apply();
        }

        time_t last_sent;
        r_last_sent.read(last_sent, 0);

        if ((current_time - last_sent > TIME_TOL) &&
            (current_time - time_1 < TIME_TOL) &&
            (current_time - time_2 < TIME_TOL)) {
            hdr.local.setInvalid();
            hdr.gen.setInvalid();
            hdr.mp_reply.setValid();

            bit<16> seq;
            r_seq.read(seq, 0);
            seq = seq + 1;
            r_seq.write(0, seq);

            mp_gen_response(standard_metadata, rsp_port_1, rsp_port_2);
            r_last_sent.write(0, standard_metadata.ingress_global_timestamp);
        }
    }
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    apply {
        bit<4> outcome;
        bit<16> seq;
        bit<4> qid;
        bit<8> qseq;
        bit<4> qidp;
        bit<8> qseqp;

        if (hdr.mp_reply.isValid()) {
            r_result.read(outcome, 0);

            r_qid.read(qid, (bit<32>)standard_metadata.egress_port);
            r_qseq.read(qseq, (bit<32>)standard_metadata.egress_port);

            r_qid.read(qidp, (bit<32>)standard_metadata.other_port);
            r_qseq.read(qseqp, (bit<32>)standard_metadata.other_port);

            r_seq.read(seq, 0);

            if ((qid != qidp) || (qseq != qseqp)) {
                hdr.mp_reply.outcome = QUEUE_MISMATCH;
            } else {
                hdr.mp_reply.outcome = outcome;
            }

            hdr.mp_reply.seq = seq;
            hdr.mp_reply.qid = qid;
            hdr.mp_reply.qseq = qseq;
            hdr.mp_reply.qidp = qidp;
            hdr.mp_reply.qseqp = qseqp;
        }
    }
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers  hdr, inout metadata meta) {
    apply { }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.mp_reply);
    }
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
    MyParser(),
    MyVerifyChecksum(),
    MyIngress(),
    MyEgress(),
    MyComputeChecksum(),
    MyDeparser()
) main;
