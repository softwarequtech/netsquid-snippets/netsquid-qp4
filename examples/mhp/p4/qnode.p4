/* -*- P4_16 -*- */
#include <core.p4>
#include <v1qr.p4>

typedef bit<48> time_t;

const bit<9> LOCAL = 0x00;

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/


header local_t {
    bit<8> cycle;
}

header gen_t {
    bit<12> _header;
    bit<4> qid;
    bit<8> qseq;
    bit<8> _unused;
}

header mp_reply_t {
    bit<12> _header;
    bit<4> outcome;
    bit<16> seq;
    bit<4> qid;
    bit<8> qseq;
    bit<4> qidp;
    bit<8> qseqp;
    bit<8> _unused;
}

struct metadata {
    /* empty */
}

struct headers {
    local_t local;
    gen_t gen;
    mp_reply_t mp_reply;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

register<bit<8>>(1) r_cycle;

parser MyParser(
    packet_in packet,
    out headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {

    state start {
        transition select(standard_metadata.ingress_port) {
            LOCAL:   parse_local;
            default: parse_mp_reply;
        }
    }

    state parse_local {
        packet.extract(hdr.local);
        transition accept;
    }

    state parse_mp_reply {
        packet.extract(hdr.mp_reply);
        transition accept;
    }

}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}


/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    action drop() {
        mark_to_drop(standard_metadata);
    }

    action gen(bit<9> egress_spec, bit<4> qid, bit<8> qseq) {
        standard_metadata.egress_spec = egress_spec;

        hdr.local.setInvalid();
        hdr.gen.setValid();

        hdr.gen.qid = qid;
        hdr.gen.qseq = qseq;

        generate_spin_photon(egress_spec);
    }

    table gen_tbl {
        key ={
            hdr.local.cycle: exact;
        }
        actions = {
            gen;
            drop;
        }
    }

    apply {
        if (hdr.local.isValid()) {

            bit<8> cycle;
            r_cycle.read(cycle, 0);
            cycle = cycle + 1;
            r_cycle.write(0, cycle);

            hdr.local.cycle = cycle;

            gen_tbl.apply();
        }
    }
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(
    inout headers hdr,
    inout metadata meta,
    inout standard_metadata_t standard_metadata
) {
    apply { }
}

/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers  hdr, inout metadata meta) {
    apply { }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.gen);
    }
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
    MyParser(),
    MyVerifyChecksum(),
    MyIngress(),
    MyEgress(),
    MyComputeChecksum(),
    MyDeparser()
) main;
