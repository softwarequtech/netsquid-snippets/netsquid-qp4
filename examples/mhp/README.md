# Midpoint Heralding Protocol example

This example illustrates a rudimentary implementation of the Midpoint Heralding
Protocol (MHP).

## Topology

```
 ---------                   --------------                   ---------
|    0x001|--- Classical ---|0x001    0x002|--- Classical ---|0x001    |
| RA      |                 |      MP      |                 |      RB |
|    0x101|--- Quantum   ---|0x101    0x102|--- Quantum   ---|0x101    |
 ---------                   --------------                   ---------
```

The two nodes on either end are connected to the local hardware timer on port
`0x000`. The midpoint is connected to its detectors on port `0x000`.

## Instructions

Run this example from the top-level directory:
```
python3 -m examples.mhp.run
```

The nodes (RA and RB) will load the program from
[`p4/qnode.json`](p4/qnode.json) which was compiled from
[`p4/qnode.p4`](p4/qnode.p4).

The midpoint station (MP) will load the program from
[`p4/midpoint.json`](p4/midpoint.json) which was compiled from
[`p4/midpoint.p4`](p4/midpoint.p4).

## What you will see

All output is available in `logs/RA.log`, `logs/RB.log`, and `logs/MP.log`
(relative to repository root).

Every 10 microseconds, the two nodes on either side receive a timer packet on
port `0x000`. Upon receipt of this packet the nodes check the `gen_tbl` to see
if there is a scheduled entry for that particular cycle. If there is one, a
photon is emitted and a packet with control information for that photon is sent
to the midpoint.

The midpoint receives notifications from the detector with the measurement
result (or an indication of failure). When it receives all three: the two
control packets from the two nodes and the detector message, it will "herald" a
response to the two nodes. Note that if the identifiers `qid` and `qseq` don't
match, the midpoint will indicate failure notifying the nodes they are not
synchronised in their entanglement attempts.

In this example we only run 10 cycles. In the first 5 cycles the entries match
on both nodes, in the last 5 cycles, the entries do not match. This can be seen
in the logs for `RA` or `RB`, because in the first five cycles, the `outcome`
field in the response packet is either `0` or `1`. In the last five cycles the
`outcome` has value `4` which is the error code `QUEUE_MISMATCH`.
