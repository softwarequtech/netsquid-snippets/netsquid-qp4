# Examples

## Verify

To verify that all the examples work run (from the repository root)
```
make examples
```

## Listing

### [Ping Pong](ping_pong)

Basic example illustrating how to send/receive and process classical packets.

### [Midpoint](midpoint)

Example illustrating a midpoint station that receives qubits on quantum
channels and responds with classical messages.

### [MHP](mhp)

Rudimentary implementation of the Midpoint Heralding Protocol (MHP).
