# Netsquid QP4

[![pipeline
status](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qp4/badges/master/pipeline.svg)](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qp4/commits/master)
[![coverage
report](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qp4/badges/master/coverage.svg)](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qp4/commits/master)

## 3rd P4 Workshop in Europe (EuroP4)

This package was first presented as a demo at the 3rd P4 Workshop in Europe on 1 December 2020. The
extended abstract is available at:
- ACM DL: [A P4 Data Plane for the Quantum Internet](https://dl.acm.org/doi/10.1145/3426744.3431321)
- arXiv: [A P4 Data Plane for the Quantum Internet](https://arxiv.org/abs/2010.11263)

To explore the code and demo as it was when the demo was presented, please check out the
[`europ4-2020`](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-qp4/-/tree/europ4-2020)
tag and follow the `README.md` instructions therein.

## Pre-requisites

### NetSquid

This package requires [NetSquid](https://netsquid.org/) which requires a user account on its
[community forum](https://forum.netsquid.org/ucp.php?mode=register).

### P4C

All P4 examples and test programs have been pre-compiled using the BMv2 backend and checked into the
repository. If you wish to compile your own programs using the `v1quantum` architecture you must use
the `v1quantum` branch of the [forked compiler](https://gitlab.com/softwarequtech/p4c).

For convenience a [Dockerfile](p4c/Dockerfile) is provided in the `p4c` directory that can be used to
build a container for the forked compiler. To build, run:
```
docker build --build-arg <JOBS> -t qp4c ./p4c
```

Then, to compile a P4 file to BMv2 JSON, run:
```
docker run -v $(pwd):/workspace qp4c --target bmv2 --arch v1qr <filename>.p4 
```

## Installation

### Docker build

It is possible that NetSquid is not available for you distribution's version of Python. Therefore,
we provide a Dockerfile which should work on any platform that supports Docker.

Once you have registered on the NetSquid forum as described in the pre-requisites section you can
build the container with
```
docker build --build-arg USERNAME=<username> --build-arg PASSWORD=<password> -t qp4 .
```

Then, to open a Bash shell within the container run
```
docker run -it -v $(pwd):/opt/netsquid-qp4 qp4 bash
```

### Normal build

To install base package dependencies (`<username>` and `<password>` are your NetSquid community
forum credentials)
```
NETSQUIDPYPI_USER=<username> NETSQUIDPYPI_PWD=<password> make python-deps
```

Note, if the installation fails due to If it fails due to `#error "cysignals must be compiled
without _FORTIFY_SOURCE"`, prepend the command above with `CFLAGS="-Wp,-U_FORTIFY_SOURCE"` as
follows
```
CFLAGS="-Wp,-U_FORTIFY_SOURCE" NETSQUIDPYPI_USER=<username> NETSQUIDPYPI_PWD=<password> make python-deps
```

To install dependencies necessary for running unit tests
```
make test-deps
```

To run the tests
```
make tests
```

## Examples

This repository contains basic usage examples in the [examples](examples) directory.
